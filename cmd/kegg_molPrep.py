import os
import sys
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import rdmolops
from rdkit.Chem import rdMolDescriptors

id2smilesDb={}
smiles2idDb={}

def convert():
	files=os.listdir("../data/kegg/mols")
	#files=['C03009.mol']
	num=len(files)
	count=0
	#files=['C00677.mol']
	for fName in files:
		keggId=fName.replace(".mol","")
		print("formating",fName,keggId)
		f=open("../data/kegg/mols/%s"%(fName))
		line=f.readline()
		molArr=[]
		numR=0
		numX=0
		numSRU=0
		missingChiral=set()
		count+=1
		while line:
			if len(line)>31:
				if line[31]=="R" or line[31]=="*":
					numR+=1
				if line[31]=="X":
					numX+=1
				if line[31]=="O" and line[32]=="H":
					line="%s %s"%(line[:32],line[33:])
			#if line.startswith("> <ENTRY>"):
			#	line=f.readline()
			#	molArr.append(line)
			#	keggId=line.strip().replace("cpd:","")
			if "SRU" in line:
				numSRU+=1
			if line.startswith("$$$$"):
				count+=1
				molArr[0]="%s\n"%(keggId)
				molArr[1]="     RDKit          2D\n"
				path="../data/kegg/mols_readable/%s"%(fName)
				f_out=open(path,'w')
				for item in molArr:
					f_out.write(item)
					if item.startswith("M  END"):
						break
				f_out.close()

				try:
					mol=Chem.MolFromMolFile(path)
					smilesCan=Chem.MolToSmiles(mol,isomericSmiles=True)
					missingChiral=getMissingChiral(Chem.MolFromSmiles(smilesCan))
					if smilesCan not in smiles2idDb:
						smiles2idDb[smilesCan]=[numR,numSRU,len(missingChiral),set(),missingChiral]
					smiles2idDb[smilesCan][3].add(keggId)
					smilesNonCan=Chem.MolToSmiles(mol,isomericSmiles=False)
					molAdj=getMolAdj(mol)
					smilesCanAdj=Chem.MolToSmiles(molAdj,isomericSmiles=True)
					smilesNonCanAdj=Chem.MolToSmiles(molAdj,isomericSmiles=False)
					
					id2smilesDb[keggId]= (numR,numX,numSRU,len(missingChiral),smilesCan,smilesNonCan,smilesCanAdj,smilesNonCanAdj,";".join(str(x) for x in missingChiral))
					#if numR==0 and numX==0 and numSRU==0:
					#	Chem.MolToMolFile(mol,"../mols/%s.mol"%(keggId))
					#	mol_H=rdmolops.AddHs(mol)
					#	AllChem.Compute2DCoords(mol_H)
					#	Chem.MolToMolFile(mol_H,"../mols_H/%s.mol"%(keggId))
				except:
					id2smilesDb[keggId]=(numR,numX,numSRU,len(missingChiral),"_NA","_NA","_NA","_NA","_NA")
					if "NA" not in smiles2idDb:
						smiles2idDb["NA"]=[-1,-1,-1,set(),set()]
					smiles2idDb["NA"][3].add(keggId)
				
			#if count>100:
			#	break
			molArr.append(line)
			line=f.readline()


def getMolAdj(mol):
	molAdj=Chem.Mol(mol)
	for atom in molAdj.GetAtoms():
		if atom.GetIsAromatic():
			continue
		atomicNum=atom.GetAtomicNum()
		atom.SetFormalCharge(0)
		if atomicNum==7:
			atom.SetNumExplicitHs(0)
	return molAdj

def getMissingChiral(mol):
	missingChiral=set()
	ccs=Chem.FindMolChiralCenters(mol,force=True,includeUnassigned=True)
	for cc in ccs:
		idx=cc[0]
		atom=mol.GetAtomWithIdx(idx)
		if cc[1]=='?' and atom.GetAtomicNum()==6:
			missingChiral.add(idx)
	return missingChiral

def print_id2smilesDb():
	print("print id2smilesDb")
	f=open("../data/kegg/misc/id2smiles_db.csv",'w')
	f.write("id,numR,numX,numSRU,numMissingChiral,smilesCan,smilesNonCan,smilesCanAdj,smilesNonCanAdj,missingChiral\n")
	for id in id2smilesDb:
		data=id2smilesDb[id]
		f.write("%s,%s\n"%(id,",".join(str(x) for x in data)))
	f.close()

def print_kegg2Smiles():
	f=open("../data/kegg/misc/id2smiles_funpdb_db.csv",'w')
	f.write("id,numR,numX,numSRU,numMissingChiral,smilesCan,smilesNonCan\n")
	for id in id2smilesDb:
		data=id2smilesDb[id]
		f.write("%s,%s\n"%(id,",".join(str(x) for x in data[0:6])))
	f.close()

def print_smiles2idDb():
	print("print smiles2idDb")
	f=open("../data/kegg/misc/smiles2id_db.csv",'w')
	f.write("smilesCan,numR,numSRU,numMissingChiral,numKeggCs,keggCs,missingchiral\n")
	for canSmiles in smiles2idDb:
		data=smiles2idDb[canSmiles]
		f.write("%s,%d,%d,%d,%d,%s,%s\n"%(canSmiles,data[0],data[1],data[2],len(data[3]),";".join(data[3]),";".join(str(x) for x in data[4])))
	f.close()

def clean_up():
	if not os.path.isdir("../data/kegg/mols_readable"):
		os.mkdir("../data/kegg/mols_readable")
	for f in os.listdir("../data/kegg/mols_readable"):
		os.remove("../data/kegg/mols_readable/%s"%(f))

def main():
	clean_up()
	convert()
	print_id2smilesDb()
	print_smiles2idDb()
	print_kegg2Smiles()

if __name__ == "__main__":
    main()
