from Bio.KEGG import REST
from Bio.KEGG import Enzyme

import os
from time import sleep

errors=set()

def download_compounds():
	compounds=[]
	compounds_txt = REST.kegg_list("compound").read()
	compounds=compounds_txt.split("\n")
	#print len(compounds)
	os.system("echo "" > Compound.sdf")
	for i in range(0,len(compounds)):
		try:
			compound=compounds[i].split()[0]
			print("downloading",i,len(compounds))
			ref=compound.split(":")[1]
			name="%s.mol"%(ref)
			if not os.path.isfile(name):
				sleep(0.2)
				request = REST.kegg_get(compound,"mol")
				open(name, 'w').write(request.read())
			os.system("cat %s >> Compound.sdf"%(name))		
		except:
			continue

		#if i>10:
		#	exit()

def print_errors():
	f=open("errors.txt","w")
	for error in errors:
		f.write("%s\n"%(error))
	f.close()

def main():
	download_compounds()

if __name__=="__main__":
	main()
