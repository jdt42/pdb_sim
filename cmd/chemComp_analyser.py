import sys

path=None

out="../data/chemComp/chemComp.csv"

smilesDb={}

def set_up():
	global path
	if len(sys.argv)!=2:
		print("enter path to chemicalComponents file")
		exit()
	path=sys.argv[1]


def read_data():
	f=open(path)
	count=0
	ref=None
	
	for line in f:
		if line.startswith("data_"):
			count+=1
			if count%100==0:
				print(count)
			ref=line.strip().split("_")[1]
			#print(ref)
			if ref not in smilesDb:
				smilesDb[ref]=set()
		if "SMILES_CANONICAL" in line:
			smiles=line.split()[-1].replace('"','')
			smilesDb[ref].add(smiles)
		
	f.close()
	

def print_data():
	f=open(out,'w')
	f.write("code,canSmiles\n")
	for code in smilesDb:
		f.write("%s,%s\n"%(code,",".join(smilesDb[code])))
	f.close()


def main():
	set_up()
	read_data()
	print_data()

if __name__=="__main__":
	main()
