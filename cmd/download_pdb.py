import os
import sys
import shutil
import subprocess
from urllib.request import urlopen
import random
from datetime import datetime

pdb_path="../data/pdb"
sifts_path=None
pdb_set=set()

months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

def download_pdb():
	count=0
	numDownloaded=0
	fails=0
	pdb_list=list(pdb_set)
	random.seed(datetime.now())
	random.shuffle(pdb_list)
	#pdb_list=pdb_list[:1000]
	for pdb in pdb_list:
		print("pdb",pdb)
		count+=1
		#if count>10:
		#	break
		if len(pdb)!=4:
			print("unusual pdb",pdb)
			continue
		subDir=pdb[0]
		subSubDir=pdb[1]
		pdb_path_subDir='%s/%s'%(pdb_path,subDir)
		pdb_path_subSubDir='%s/%s'%(pdb_path_subDir,subSubDir)
		#print pdb,pdb_path_subSubDir
		if not os.path.isdir(pdb_path_subDir):
			os.mkdir(pdb_path_subDir)
		if not os.path.isdir(pdb_path_subSubDir):
			os.mkdir(pdb_path_subSubDir)
		#print "###",count,"out of:",len(pdb_set)," status: downloading ",pdb
		out_path="%s/%s.pdb" % (pdb_path_subSubDir,pdb)
		#print "out_path",out_path
		if os.path.isfile(out_path) and os.path.getsize(out_path)>0:
			print("already downloaded!",count,pdb)
			numDownloaded+=1
			continue
		try:
			#url = 'http://www.rcsb.org/pdb/files/%s.pdb'%(pdb)
			url = 'http://files.rcsb.org/view/%s.pdb'%(pdb)
			print(url)
			pdb_data = urlopen(url).read().decode('utf-8')
			numDownloaded+=1
		except:
			fails+=1
			print("##### fail",count,pdb)
			continue
		print("success!",count,pdb)
		output_pdb=open(out_path,"w")
		output_pdb.write(pdb_data)
		output_pdb.close()
	print("numInSample",numDownloaded)
	print("fails",fails)

def read_sifts():
	f = open(sifts_path,'r')
	for line in f:
		if line.startswith("#"):
			continue
		print(line)
		
		pdb=line.strip().split(',')[0]
		if ".00E+" in pdb:
			pdb=pdb.replace(".00E+","e")
		if pdb[-3:] in months:
			end=pdb[-3:].lower()
			pdb="%s%s"%(pdb[1],end)
		pdb_set.add(pdb)
	f.close()
	print("num pdb",len(pdb_set))
	

def set_up():
	global sifts_path
	if len(sys.argv)==2:
		sifts_path=sys.argv[1]
	else:
		print("enter path to sifts file")
		exit()
	print(sifts_path)

def main():
	set_up()
	read_sifts()
	print("num PDB",len(pdb_set))
	download_pdb()

if __name__ == "__main__":
	main()
