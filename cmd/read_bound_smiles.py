import os
import sys

from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors
from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem import MACCSkeys
from rdkit.Chem.AtomPairs import Pairs
from rdkit.Chem.AtomPairs import Torsions
from rdkit.Chem import MCS as mcs
#from rdkit.Chem import rdFMCS as mcs

from simtk import openmm
from simtk.openmm.app import *
from simtk.openmm import *
import scipy

useParity=0

pdbEnzyme_toRun_path="pdbEnzyme_toRun.csv"
eqn_path=None

pdb_root="../data/pdb/"
smiles_path="../data/chemComp/chemComp.csv" #"hetSmilesPdb.csv"
smiles_corrections_path="../data/pdb/smilesCorrections.csv"
mol_root=None

batch_size=None
include_markush=True
db=None

ec_kegg_rType_map={}
global_cognate_map={}
bound_smiles_map={}

cog_markush=set()

pdbEnzyme_db={}
pdb_ec_set=set()
pdb_uniprot={}
pdb_cluster={}
unmatched_bound_map={}
unmatched_pdb_ec_map={}
unmatched_ec_pdb_map={}
error_set=set()

model_res={}
model_other_res={}
model_best_pdb={}
model_best_ec={}
model_best_rxn={}

bound_cognate_map={}

global_cognate_mol_map={}
global_bound_mol_map={}
global_sim_arr_map={}

#ignore water,proton,dihydrogen
kegg_ignore=set(['C00001','C00080','C00282'])

'''
all: all bound compared to all cognate
other_x: if similarity to other is great than x match toother and not main
other_gt_main: if similarity to other is greater than main match to other and not main
'''
models=['reactant','product']#,'reactant_main','product_main']
#models=['reactant']

stdRes=["ARG","HIS","LYS","ASP","GLU","SER","THR","ASN","GLN","CYS","SEC","GLY","PRO","ALA","ILE","LEU","MET","PHE","TRP","TYR","VAL"]

peptideBond=["C","N"]

def read_bound_smiles():
	f = open(smiles_path,'r')
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr = line.strip().split(',')
		if len(arr)>=2:
			bound_smiles_map[arr[0]]=arr[1]
	f.close()

	#f_corr = open(smiles_corrections_path,'r')
	#count=0
	#for line in f_corr:
	#	count+=1
	#	if count==1:
	#		continue
	#	arr = line.strip().split(',')
	#	if len(arr)>=2:
	#		#print "adding",arr[0],arr[1]
	#		bound_smiles_map[arr[0]]=arr[1]
	#f_corr.close()
	print("num of bound to smiles",len(bound_smiles_map))

def test_smiles():
	bound_mol_map={}
	f=open("check_smiles.csv","w")
	f.write("ref,smiles\n")
	for bound_ref in bound_smiles_map:
		bound_smiles = bound_smiles_map[bound_ref]
		bound_mol=Chem.MolFromSmiles(bound_smiles)
		try:
			rdkit_can_smiles=Chem.MolToSmiles(bound_mol)
		except:
			print("check smiles",bound_ref,bound_smiles)
			f.write("%s,%s\n"%(bound_ref,bound_smiles))
	f.close()
	

def main():
	read_bound_smiles()
	test_smiles()

if __name__ == "__main__":
	main()
