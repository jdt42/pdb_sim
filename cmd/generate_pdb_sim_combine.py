import os
import sys

from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors
from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem import MACCSkeys
from rdkit.Chem.AtomPairs import Pairs
from rdkit.Chem.AtomPairs import Torsions
from rdkit.Chem import MCS

files=set()

bound_db={}
cognate_db={}
cognate_type_db={}

obsolete_new_map={}
ec_rxn_rType_map={}
global_cognate_map={}
bound_smiles_map={}

pdb_cath_map={}
pdb_set=set()
pdb_ec_set=set()
pdb_uniprot={}
pdb_cluster={}
unmatched_bound_map={}
unmatched_pdb_ec_map={}
unmatched_ec_pdb_map={}
error_set=set()

model_res={}
model_other_res={}
model_best_pdb={}
model_best_ec={}
model_best_rxn={}

model_rank_pdb={}
model_rank_ec={}
model_rank_rxn={}

global_cognate_mol_map={}
global_bound_mol_map={}
global_sim_arr_map={}

#ignore water,proton,dihydrogen
rxn_ignore=set(['C00001','C00080','C00282'])

'''
all: all bound compared to all cognate
other_x: if similarity to other is great than x match toother and not main
other_gt_main: if similarity to other is greater than main match to other and not main
'''
models=['reactant','product']#,'reactant_main','product_main']
#models=['reactant']


def print_sim_res_line(f,key,model):
	pdb=key[0]
	ec=key[1]
	ec_c=ec.split('.')[0]
	rxn=key[2]
	rxn_strip=rxn.replace('_r','').replace('_p','')
	arr=model_res[model][key]

	cpd_sim_arr=model_res[model][key][0]
	rxn_sim=model_res[model][key][1]

	rxn_sim_bin=round(2*rxn_sim,1)/2
	is_best_pdb = model_best_pdb[model][pdb][0]==key
	if ec in model_best_ec[model]:
		is_best_ec = model_best_ec[model][ec][0]==key
	else:
		is_best_ec = False

	if rxn_strip in model_best_rxn[model]:
		is_best_rxn = model_best_rxn[model][rxn_strip][0]==key
	else:
		is_best_rxn = False

	if is_best_pdb or is_best_ec or is_best_rxn:
		rank=0
		prev_sim=-1
		markush=0
		for i in range(0,len(cpd_sim_arr)):
			cpd_sim=cpd_sim_arr[i]
			cognate=cpd_sim[0][1]
			if cognate.endswith('R'):
				markush=1
		for i in range(0,len(cpd_sim_arr)):
			cpd_sim=cpd_sim_arr[i]
			bound=cpd_sim[0][0]
			bound_bonds='NA'
			bound_rot='NA'
			if bound in bound_db:
				bound_bonds=bound_db[bound][0]
				bound_rot=bound_db[bound][1]
			cognate=cpd_sim[0][1]
			cog_bonds='NA'
			cog_rot='NA'
			if cognate in cognate_db:
				cog_bonds=cognate_db[cognate][0]
				cog_rot=cognate_db[cognate][1]
			cognate_type=cognate_type_db[(pdb,ec,rxn,cognate)]
			counts=cpd_sim[1]
			bound_num=counts[0]
			cognate_num=counts[1]
			mcs_num=counts[2]
			sim=cpd_sim[2]
			#print cpd_sim
			sim_bin=round(2*sim,1)/2
			#if sim<>prev_sim:
			rank+=1
			prev_sim=sim
			#print cognate,bound,rank
			f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.3f,%.3f,%.3f,%.3f,%d,"%(pdb,ec,ec_c,rxn,bound,cognate,cognate_type,bound_num,cognate_num,mcs_num,rxn_sim,rxn_sim_bin,sim,sim_bin,rank))
			f.write("%d,%d,%d,"%(is_best_pdb,is_best_ec,is_best_rxn))
			f.write("%s,%s,%s,%s,%d\n"%(bound_rot,bound_bonds,cog_rot,cog_bonds,markush))

def update_best_maps(map,model,key,res_key,res_list,score):
	#print "\nupdate map",type,key,res_key,res
	if key not in map[model]:
		#print "add new",res_key,mcs_res
		map[model][key]=(res_key,score)
	elif score>=map[model][key][1]:
		#print "improve",mcs_res,map[type][key][1]
		map[model][key]=(res_key,score)
	#print "end",type,key,map[type][key]



def generate_reaction_score(l):
	matches=0
	atoms=0
	for i in range(0,len(l)):
		counts=l[i][1]
		if counts[1]!='NA':
			atoms+=counts[0]
			atoms+=counts[1]
			matches+=counts[2]
	rxn_score=0
	if (atoms-matches)==0:
		rxn_score=0
	else:
		rxn_score=float(matches)/float(atoms-matches)
	return rxn_score

def process_file(f_name):
	f=open("sim/%s"%f_name)
	count=0
	res_map={}
	for line in f:
		count+=1
		if count==1:
			head_arr=line.strip().split(',')
			for i in range(0,len(head_arr)):
				j=1
				#print i,head_arr[i]
			continue
		arr=line.strip().split(',')
		pdb=arr[0]
		ec=arr[1]
		rxn=arr[3]
		bound=arr[4]
		cognate=arr[5]
		cog_type=arr[6]
		bound_num=arr[7]
		if bound_num!='NA':
			bound_num=int(bound_num)
		cognate_num=arr[8]
		if cognate_num!='NA':
			cognate_num=int(cognate_num)
		ucs_num=int(arr[9])
		rxn_sim=float(arr[10])		
		cpd_sim=float(arr[12])
		bound_rot=arr[18]
		bound_bonds=arr[19]
		cog_rot=arr[20]
		cog_bonds=arr[21]
		markush=arr[22]
		cognate_type_db[(pdb,ec,rxn,cognate)]=cog_type
		cognate_db[cognate]=(cog_bonds,cog_rot)
		bound_db[bound]=(bound_bonds,bound_rot)
		res_key=(pdb,ec,rxn)
		if res_key not in res_map:
			res_map[res_key]=[]
		key=(bound,cognate)
		cpd_sim_res=(key,(bound_num,cognate_num,ucs_num),cpd_sim)
		res_map[res_key].append(cpd_sim_res)
	return res_map
	
def process_res_map(res_map,model):
	for res_key in res_map:
		pdb=res_key[0]
		ec=res_key[1]
		rxn=res_key[2]
		rxn_strip=rxn.replace('_r','').replace('_p','')

		ec_c=ec.split('.')[0]
		sim_list=res_map[res_key]
		sim_score=generate_reaction_score(sim_list)
		model_res[model][res_key]=(sim_list,sim_score)
				
		update_best_maps(model_best_pdb,model,pdb,res_key,sim_list,sim_score)
		if '-' not in ec:
			update_best_maps(model_best_ec,model,ec,res_key,sim_list,sim_score)
		update_best_maps(model_best_rxn,model,rxn_strip,res_key,sim_list,sim_score)

def set_to_str(s):
	str=""
	l=list(s)
	l.sort()
	for item in l:
		str=str+item+"_"
	str=str[:-1]
	return str

def print_sim_res():
	for model in model_res:
		print("printing sim_res",model)
		model_f=open("pdb_sim_%s.csv"%(model),"w")
		model_f.write("pdb,ec,ec_c,rxn,bound,cognate,cognate_type,bound_num,cognate_num,mcs_num,rxn_sim,rxn_sim_bin,cpd_sim,cpd_sim_bin,cpd_rank,")
		model_f.write("is_best_pdb,is_best_ec,is_best_rxn,")
		model_f.write("bound_rot,bound_bonds,cog_rot,cog_bonds,markush\n")
		for key in model_res[model]:
			print_sim_res_line(model_f,key,model)
		model_f.close()

def print_admin_files():
	suffix=""
	if batch!=0:
		suffix="_"+str(batch)
	error_f=open("errors_pdb_sim%s.csv"%(suffix),"w")
	for pdb in error_set:
		error_f.write("%s\n"%pdb)
	error_f.close()

	unmatched_bound_f=open("unmatched_bound%s.csv"%(suffix),"w")
	unmatched_bound_f.write("bound,pdb\n")
	for bound in unmatched_bound_map:
		pdb_str=set_to_str(unmatched_bound_map[bound])
		unmatched_bound_f.write("%s,%s\n"%(bound,pdb_str))
	unmatched_bound_f.close()

	unmatched_pdb_f=open("unmatched_pdb%s.csv"%(suffix),"w")
	unmatched_pdb_f.write("pdb,ec\n")
	for pdb in unmatched_pdb_ec_map:
		unmatched_pdb_f.write("%s,%s\n"%(pdb,set_to_str(unmatched_pdb_ec_map[pdb])))
	unmatched_pdb_f.close()

	unmatched_ec_f=open("unmatched_ec%s.csv"%(suffix),"w")
	unmatched_ec_f.write("ec,pdb\n")
	for ec in unmatched_ec_pdb_map:
		unmatched_ec_f.write("%s,%s\n"%(ec,set_to_str(unmatched_ec_pdb_map[ec])))
	unmatched_ec_f.close()

def read_errors(f_name):
	f=open("sim/%s"%(f_name),'r')
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		error_set.add(line.strip())
	f.close()	

def read_admin_file(f_name,map):
	f=open("sim/%s"%(f_name),'r')
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')
		key=arr[0]
		res=arr[1].split('_')
		if key not in map:
			map[key]=set()
		map[key].update(res)
	f.close()

def process_rank_file(f_name,model,name):
	if name == "pdb":
		rank_map=model_rank_pdb[model]
	elif name == "ec":
		rank_map=model_rank_ec[model]	
	elif name == "rxn":
		rank_map=model_rank_rxn[model]
	else:
		print("mismatch name",name)
	f=open("sim/%s"%f_name)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')
		key=arr[1]
		res_key=(arr[2],arr[4],arr[5])
		ec_sifts=arr[3]
		r_or_p=arr[6]
		sim=float(arr[7])
		r_or_p=arr[6]	
		markush=arr[8]
		cpd_str=arr[9]
		if key not in rank_map:
			rank_map[key]=[]
		rank_map[key].append((res_key,sim,markush,cpd_str,r_or_p,ec_sifts))
	f.close()

def process_bound_cog_sim_file(f_name):
	f=open("sim/%s"%f_name)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')
		key=(arr[0],arr[1])
		res=(int(arr[2]),int(arr[3]),int(arr[4]),float(arr[5]))
		global_sim_arr_map[key]=res
	f.close()
	#print len(global_sim_arr_map)

def sort_rank_map(rank_map):
	for model in rank_map.keys():
		for key in rank_map[model].keys():
			print("sorting",model,key)
			rank_map[model][key].sort(key=lambda x: x[1],reverse=True)

def sort_rank_maps():
	sort_rank_map(model_rank_pdb)
	sort_rank_map(model_rank_ec)
	sort_rank_map(model_rank_rxn)

def combine_sim_res(files):
	# amend
	num_pdb=len(pdb_set)
	count=0

	#files=set(["pdb_global_stats_all_1.csv"])
	for f_name in files:
		print("processing",f_name)
		if "pdb_sim" in f_name:
			model="_".join(f_name.replace("pdb_sim_","").split("_")[:-1])
			#print f_name,model
			res_map=process_file(f_name)
			#print "num res",len(res_map)
			process_res_map(res_map,model)
		elif "rank_stats" in f_name:
			model="_".join(f_name.replace("rank_stats_","").split("_")[1:-1])
			name="_".join(f_name.replace("rank_stats_","").split("_")[:1])
			#print model,name
			process_rank_file(f_name,model,name)
		elif "bound_cog_sim" in f_name:
			process_bound_cog_sim_file(f_name)
		elif "unmatched_pdb" in f_name:
			read_admin_file(f_name,unmatched_pdb_ec_map)
		elif "unmatched bound" in f_name:
			read_admin_file(f_name,unmatched_bound_map)
		elif "unmatched_ec" in f_name:
			read_admin_file(f_name,unmatched_ec_pdb_map)	
		elif "errors" in f_name:
			read_errors(f_name)
		else:
			continue
	sort_rank_maps()
	print_sim_res()
	print_ranks()
	print_cpd_sim()
	print_admin_files()


def print_rank(model_rank,name):
	for model in model_rank:
		print("printing rank",name,model)
		model_f=open("rank_stats_%s_%s.csv"%(name,model),"w")
		model_f.write("rank,key,pdb,ec_sifts,ec_rxn,rxn,r_or_p,rxn_sim,markush,cpd_str\n")
		for key in model_rank[model]:
			res_list=model_rank[model][key]
			prev_sim=None
			rank=1
			for i in range(0,len(res_list)):
				#(res_key,sim,markush,cpd_str,r_or_p,ec_sifts)
				res=res_list[i]
				res_key=res[0]
				sim=res[1]
				if sim!=prev_sim:
					rank=i+1
				prev_sim=sim	
				markush=res[2]
				cpd_str=res[3]
				r_or_p=res[4]
				ec_sifts=res[5]
				model_f.write("%d,%s,%s,%s,%s,%s,%s,%.3f,%s,%s\n"%(rank,key,res_key[0],ec_sifts,res_key[1],res_key[2],r_or_p,sim,markush,cpd_str))
		model_f.close()


def print_ranks():
        print_rank(model_rank_pdb,"pdb")
        print_rank(model_rank_ec,"ec")
        print_rank(model_rank_rxn,"rxn")

def print_cpd_sim():
	print("printing cpd_sim")
	f=open("bound_cog_sim.csv",'w')
	f.write("bound,cognate,bound_num,cognate_num,mcs_num,sim\n")
	for key in global_sim_arr_map:
		bou=key[0]
		cog=key[1]
		sim_arr=global_sim_arr_map[key]
		bou_atoms=sim_arr[0]
		cog_atoms=sim_arr[1]
		matches=sim_arr[2]
		sim=sim_arr[3]
		f.write("%s,%s,%d,%d,%d,%.3f\n"%(bou,cog,bou_atoms,cog_atoms,matches,sim))
	f.close()


def set_up():
	global batch
	batch=0
	if len(sys.argv)==2:
		batch=int(sys.argv[1])
	if batch!=0:
		read_config()
	for model in models:
		model_res[model]={}
		model_best_pdb[model]={}
		model_best_ec[model]={}
		model_best_rxn[model]={}

		model_rank_pdb[model]={}
		model_rank_ec[model]={}
		model_rank_rxn[model]={}

	for model_summary in ['all']:#['main','all']:
		model_res[model_summary]={}
		model_best_pdb[model_summary]={}
		model_best_ec[model_summary]={}
		model_best_rxn[model_summary]={}
		
		model_rank_pdb[model_summary]={}
		model_rank_ec[model_summary]={}
		model_rank_rxn[model_summary]={}

def read_files():
	files=os.listdir("sim")
	print("number of files",len(files))
	return files

def main():
	print("in main")
	set_up()
	files=read_files()
	combine_sim_res(files)

if __name__ == "__main__":
	main()
