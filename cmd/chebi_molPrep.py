import os
import sys
from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import rdmolops
from rdkit.Chem import rdMolDescriptors

sdfPath=None
id2smilesDb={}
smiles2idDb={}

def set_up():
	global sdfPath
	if len(sys.argv)!=2:
		print("enter path to sdf file")
		exit()
	sdfPath=sys.argv[1]


def convert():
	f=open(sdfPath)
	line=f.readline()
	molArr=[]
	numR=0
	numX=0
	numSRU=0
	missingChiral=set()
	count=0
	while line:
		
		if len(line)>31:
			if line[31]=="R" or line[31]=="*":
				numR+=1
				#if line[32]=="#":
				#	line="%s %s"%(line[:32],line[33:])
			if line[31]=="X":
				numX+=1
		molArr.append(line)
		if line.startswith("> <ChEBI ID>"):
			line=f.readline()
			molArr.append(line)
			chebiId=line
		if "SRU" in line:
			numSRU+=1
		if "MON" in line:
			numSRU+=1
		if "COP" in line:
			numSRU+=1
		if line.startswith("$$$$"):
			count+=1
			chebiNum=int(chebiId.strip().split(":")[1])
			molArr[0]="%s"%(chebiId)
			molArr[1]="     RDKit          2D\n"
			path="../data/chebi/mols/%d.mol"%(chebiNum)
			pathReadable="../data/chebi/mols_readable/%d.mol"%(chebiNum)
			f_out=open(path,'w')
			for item in molArr:
				f_out.write(item)
				if item.startswith("M  END"):
					break
			f_out.close()
			
			try:
				mol=Chem.MolFromMolFile(path)
				for atom in mol.GetAtoms():
					if atom.GetAtomicNum()==7 and atom.GetFormalCharge()==1 and atom.GetTotalNumHs()>2:
						atom.SetFormalCharge(0)
						print("######## deprotonate charged N",path)
					if atom.GetAtomicNum()==8 and atom.GetFormalCharge()==-1 and atom.GetTotalNumHs()==0:
						atom.SetFormalCharge(0)
						print("######## protonate charged O",path)
				mol_noH=rdmolops.RemoveHs(mol)
				Chem.MolToMolFile(mol_noH,pathReadable)
			except:
				print("error",path,numR,numSRU)
				#Chem.MolToMolFile(mol,path)
				
			try:
				mol=Chem.MolFromMolFile(pathReadable)
				smilesCan=Chem.MolToSmiles(mol,isomericSmiles=True)
				missingChiral=getMissingChiral(Chem.MolFromSmiles(smilesCan))
				if smilesCan not in smiles2idDb:
					smiles2idDb[smilesCan]=[numR,numSRU,len(missingChiral),set(),missingChiral]
				smiles2idDb[smilesCan][3].add(chebiNum)
				smilesNonCan=Chem.MolToSmiles(mol,isomericSmiles=False)
				molAdj=getMolAdj(mol)
				smilesCanAdj=Chem.MolToSmiles(molAdj,isomericSmiles=True)
				smilesNonCanAdj=Chem.MolToSmiles(molAdj,isomericSmiles=False)
				
				id2smilesDb[chebiNum]=(numR,numX,numSRU,len(missingChiral),smilesCan,smilesNonCan,smilesCanAdj,smilesNonCanAdj, ";".join(str(x) for x in missingChiral))
				#mol_H=rdmolops.AddHs(mol)
				#AllChem.Compute2DCoords(mol_H)
				#Chem.MolToMolFile(mol_H,"../mols_H/%d.mol"%(chebiNum))
			except:
				id2smilesDb[chebiNum]=(numR,numX,numSRU,len(missingChiral),"_NA","_NA","_NA","_NA","_NA")
				if "NA" not in smiles2idDb:
					smiles2idDb["NA"]=[-1,-1,-1,set(),set()]
				smiles2idDb["NA"][3].add(chebiNum)
			molArr=[]
			numR=0
			numX=0
			numSRU=0
			missingChiral=set()
			if count%1000==0:
				print("analysing",count)
				#break
		#if count>100:
		#	break

		line=f.readline()

def getMolAdj(mol):
	molAdj=Chem.Mol(mol)
	for atom in molAdj.GetAtoms():
		if atom.GetIsAromatic():
			continue
		atomicNum=atom.GetAtomicNum()
		atom.SetFormalCharge(0)
		if atomicNum==7:
			atom.SetNumExplicitHs(0)
	return molAdj

def getMissingChiral(mol):
	missingChiral=set()
	ccs=Chem.FindMolChiralCenters(mol,force=True,includeUnassigned=True)
	for cc in ccs:
		idx=cc[0]
		atom=mol.GetAtomWithIdx(idx)
		if cc[1]=='?' and atom.GetAtomicNum()==6:
			missingChiral.add(idx)
	return missingChiral

def print_id2smilesDb():
	print("print id2smilesDb")
	f=open("../data/chebi/misc/id2smiles_db.csv",'w')
	f.write("id,numR,numX,numSRU,numMissingChiral,smilesCan,smilesNonCan,smilesCanAdj,smilesNonCanAdj,missingChiral\n")
	for id in id2smilesDb:
		data=id2smilesDb[id]
		f.write("%d,%s\n"%(id,",".join(str(x) for x in data)))
	f.close()

def print_smiles2idDb():
	print("print smiles2idDb")
	f=open("../data/chebi/misc/smiles2id_db.csv",'w')
	f.write("smilesCan,numR,numSRU,numMissingChiral,numChebis,chebis,missingChiral\n")
	for canSmiles in smiles2idDb:
		data=smiles2idDb[canSmiles]
		f.write("%s,%d,%d,%d,%d,%s,%s\n"%(canSmiles,data[0],data[1],data[2],len(data[3]),";".join(str(x) for x in data[3]),";".join(str(x) for x in data[4])))
	f.close()

def main():
	set_up()
	convert()
	print_id2smilesDb()
	print_smiles2idDb()

if __name__=="__main__":
	main()
