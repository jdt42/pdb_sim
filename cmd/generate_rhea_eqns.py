import os
import sys

rxnPath="../data/rhea/rxns"
rheaEqns={}
chebiMols="../data/chebi/mols_readable/"
#rxnOutPath="../data/rhea/rxns"
chebiSmilesPath="../data/chebi/misc/id2smiles_db.csv"
eqnPath="../data/rhea/misc/equations.csv"
ecChebiRheaPath="../data/rhea/misc/ecChebiRhea.csv"
rhea2ec_path="../data/rhea/rhea2ec.tsv"
smilesDb={}
r_ec_map={}
ecChebiRhea={}

def readRxns():
	rxns=[f for f in os.listdir(rxnPath) if f.endswith('.rxn')]
	for rxn in rxns:
		print("reading rxn",rxn)
		rheaId=int(rxn.replace(".rxn",""))
		numArr=[]
		chebiArr=[]
		f=open("%s/%s"%(rxnPath,rxn))
		line=f.readline()
		count=0
		while line:
			#print(count,line)
			if count==4:
				numR=int(line[:3].strip())
				numP=int(line[3:].strip())
				numArr=[numR,numP]
			if line.startswith("CHEBI"):
				chebiArr.append(int(line.strip().split(":")[1]))
			line=f.readline()
			count+=1
		#print(numArr)
		#print(chebiArr)
		rheaEqns[rheaId]=[numArr,chebiArr]
		f.close()

def genEcChebiRhea():
	rheaIds=list(rheaEqns.keys())
	rheaIds.sort()
	for rheaId in rheaIds:
		if rheaId in r_ec_map:
			ecs=r_ec_map[rheaId]
		else:
			ecs=set(["NA"])
		chebis=rheaEqns[rheaId][1]
		print(rheaId,ecs,chebis)
		for ec in ecs:
			if ec not in ecChebiRhea:
				ecChebiRhea[ec]={}
			for chebi in chebis:
				if chebi not in ecChebiRhea[ec]:
					ecChebiRhea[ec][chebi]=set()
				ecChebiRhea[ec][chebi].add(rheaId)

def printEcChebiRhea():
	f=open(ecChebiRheaPath,'w')
	f.write("ec,chebiId,rhea\n")
	ecs=list(ecChebiRhea.keys())
	ecs.sort()
	for ec in ecs:
		chebis=list(ecChebiRhea[ec])
		chebis.sort()
		for chebi in chebis:
			rheas=list(ecChebiRhea[ec][chebi])
			rheas.sort()
			f.write("%s,%d,%s\n"%(ec,chebi,";".join(str(x) for x in rheas)))
	f.close()

def printEqns():
	f=open(eqnPath,'w')
	f.write("rheaId,numR,numP,rs,ps,ecs\n")
	rheaIds=list(rheaEqns.keys())
	rheaIds.sort()
	for rheaId in rheaIds:
		data=rheaEqns[rheaId]
		#print(rheaId,data)
		numR=data[0][0]
		numP=data[0][1]
		chebis=data[1]
		ecs=[]

		if rheaId in r_ec_map:
			ecs=list(r_ec_map[rheaId])
			ecs.sort()
		
		f.write("%d,%d,%d,%s,%s,%s\n"%(rheaId,numR,numP,";".join(str(x) for x in chebis[:numR]),";".join(str(x) for x in chebis[numR:]),";".join(ecs)))
	f.close()

def formatStr(num,l):
	numStr=str(num)
	while len(numStr)<l:
		numStr=" "+numStr
	return numStr

def genRxns():
	rheaIds=list(rheaEqns.keys())
	rheaIds.sort()
	for rheaId in rheaIds:
		print("generating",rheaId)
		rxnContainsMarkush=False
		rxnIsPolymer=False
		rxnIsComplete=True
		data=rheaEqns[rheaId]
		numR=data[0][0]
		numP=data[0][1]
		chebis=data[1]
		count=0
		numRmissing=0
		numPmissing=0
		for chebi in chebis:
			count+=1
			chebiPath="%s%s.mol"%(chebiMols,chebi)
			if not os.path.isfile(chebiPath):
				rxnIsComplete=False
				if count<=numR:
					numRmissing+=1
				else:
					numPmissing+=1
		numR=numR-numRmissing
		numP=numP-numPmissing
		path="%s/pass/%s.rxn"%(rxnOutPath,rheaId)
		f=open(path,'w')
		f.write("$RXN\n\nrhea\t%s\n\n"%(rheaId))
		numRstr=formatStr(numR,3)
		numPstr=formatStr(numP,3)
		f.write("%s%s\n"%(numRstr,numPstr))
		for chebi in chebis:
			chebiPath="%s%s.mol"%(chebiMols,chebi)
			if not os.path.isfile(chebiPath):
				continue
			if int(smilesDb[chebi][0])>0:
				rxnContainsMarkush=True
			if int(smilesDb[chebi][2])>0:
				rxnIsPolymer=True
			f.write("$MOL\n")
			fChebi=open(chebiPath)
			lines=fChebi.readlines()
			for line in lines:
				f.write(line)
			fChebi.close()
		f.close()
		if rxnContainsMarkush:
			os.rename(path,"%s/containsMarkush/%s.rxn"%(rxnOutPath,rheaId))
		elif rxnIsPolymer:
			os.rename(path,"%s/polymer/%s.rxn"%(rxnOutPath,rheaId))
		elif not rxnIsComplete:
			os.rename(path,"%s/incomplete/%s.rxn"%(rxnOutPath,rheaId))

def readSmilesDb():
	f=open(chebiSmilesPath)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')
		smilesDb[int(arr[0])]=arr[1:]
	f.close()
	print("numSmiles",len(smilesDb))

def readRhea2ec():
	f=open(rhea2ec_path)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split()
		if len(arr)!=4:
			continue
		rheaId=int(arr[0])
		ec=arr[3]
		rheaIdForward=rheaId+1
		rheaIdRetro=rheaId+2
		
		if rheaIdForward not in r_ec_map:
			r_ec_map[rheaIdForward]=set()
		if rheaIdRetro not in r_ec_map:
			r_ec_map[rheaIdRetro]=set()
		r_ec_map[rheaIdForward].add(ec)
		r_ec_map[rheaIdRetro].add(ec)
	print("r_ec",len(r_ec_map))

def set_up():
	global rxnPath
	if len(sys.argv)!=2:
		print("enter path to rxns")
		exit()
	rxnPath=sys.argv[1]

def main():
	#set_up()
	readRhea2ec()
	readRxns()
	printEqns()
	genEcChebiRhea()
	printEcChebiRhea()
	readSmilesDb()
	#genRxns()


if __name__=="__main__":
	main()
