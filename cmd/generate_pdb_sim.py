import os
import sys

from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors
from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem import MACCSkeys
from rdkit.Chem.AtomPairs import Pairs
from rdkit.Chem.AtomPairs import Torsions
from rdkit.Chem import MCS as mcs
#from rdkit.Chem import rdFMCS as mcs

from simtk import openmm
from simtk.openmm.app import *
from simtk.openmm import *
import scipy

useParity=0

pdbEnzyme_toRun_path="pdbEnzyme_toRun.csv"
eqn_path=None

pdb_root="../data/pdb/"
pdb_bound_path="../data/pdb/pdb_bound.csv"
smiles_path="../data/chemComp/chemComp.csv" #"hetSmilesPdb.csv"
smiles_corrections_path="../data/pdb/smiles_corrections.csv"
mol_root=None

batch_size=None
include_markush=True
db=None

pdb_bound_db={}
ec_rxn_rType_map={}
global_cognate_map={}
bound_smiles_map={}

cog_markush=set()

pdb_bound_db={}
pdbEnzyme_db={}
pdb_ec_set=set()
pdb_uniprot={}
pdb_cluster={}
unmatched_bound_map={}
unmatched_pdb_ec_map={}
unmatched_ec_pdb_map={}
error_set=set()

model_res={}
model_other_res={}
model_best_pdb={}
model_best_ec={}
model_best_rxn={}

bound_cognate_map={}

global_cognate_mol_map={}
global_bound_mol_map={}
global_sim_arr_map={}

#ignore water,proton,dihydrogen
rxn_ignore=set(['C00001','C00080','C00282'])

'''
all: all bound compared to all cognate
other_x: if similarity to other is great than x match toother and not main
other_gt_main: if similarity to other is greater than main match to other and not main
'''
models=['reactant','product']#,'reactant_main','product_main']
#models=['reactant']

stdRes=["ARG","HIS","LYS","ASP","GLU","SER","THR","ASN","GLN","CYS","SEC","GLY","PRO","ALA","ILE","LEU","MET","PHE","TRP","TYR","VAL"]

def has_structure(cpds):
	for cpd in cpds:
		if cpd in rxn_ignore:
			continue
		if os.path.isfile("%s/%s.mol"%(mol_root,cpd)):
			return True
	return False	

def read_eqns():
	f=open(eqn_path)
	count=0
	#errorCount=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')
		rxn=arr[0]
		reactants=arr[3].split(';')
		products=arr[4].split(';')
		ecs=arr[5].split(';')
		if len(ecs)==0:
			continue	
		for ec in ecs:
			#ecSplit=ec.split(".")
			#if "-" in ec:
			#	errorCount+=1
			#	print errorCount,rxn,ecs		
			if ec not in ec_rxn_rType_map:
				ec_rxn_rType_map[ec]={}
			
			ec_rxn_rType_map[ec][rxn]=(reactants,products)
	print("num ec",len(ec_rxn_rType_map))

def read_bound_smiles():
	f = open(smiles_path,'r')
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr = line.strip().split(',')
		if len(arr)>=2:
			bound_smiles_map[arr[0]]=arr[1]
	f.close()

	f_corr = open(smiles_corrections_path,'r')
	count=0
	for line in f_corr:
		count+=1
		if count==1:
			continue
		arr = line.strip().split(',')
		if len(arr)>=2:
			#if arr[0]=="HEM":
			#	print("adding",arr[0],arr[1])
			bound_smiles_map[arr[0]]=arr[1]
	f_corr.close()
	print("num of bound to smiles",len(bound_smiles_map))

def get_cpd_mol_block(cpd):
	rxn_path="%s/%s.mol"%(mol_root,cpd)
	mol_block_arr=None
	if os.path.isfile(rxn_path):
		f_mol=open(rxn_path)
		mol_block_arr=f_mol.readlines()
	return mol_block_arr

def get_cognate_mol(cpd):
	rxn_path="%s/%s.mol"%(mol_root,cpd)
	mol=None
	if os.path.isfile(rxn_path):
			#try:
			#	mol=Chem.MolFromMolFile(rxn_path)
	
			try:
				f=open(rxn_path)
				lines=f.readlines()
				molblock=""
				for line in lines:
					if len(line)>31 and line[31]=='X':
						line="%sF%s"%(line[0:31],line[32:])
					molblock="%s%s"%(molblock,line)					
				mol=Chem.MolFromMolBlock(molblock)	
			except:
				mol=None
	return mol


def replace_R_with_H(cpd_mol_block_arr):
	cpd_mol_block=""
	found_R=False
	for line in cpd_mol_block_arr:
		arr=line.split()
		if len(arr)>3 and arr[3]=="R":
			found_R=True
			line=line.replace("R","H")
		cpd_mol_block=cpd_mol_block+line
	return cpd_mol_block,found_R

def get_matches(mol,smarts):
	#print "mol",Chem.MolToSmiles(mol)
	#print "smarts",smarts
	patt=Chem.MolFromSmarts(smarts)
	#print Chem.MolToSmiles(patt)
	#JDTmatches=mol.GetSubstructMatches(patt)
	matches=mol.GetSubstructMatches(patt,uniquify=False)
	#print matches
	return matches

def get_symbol(atom):
	symbol=atom.GetSymbol()
	#if atom.GetIsAromatic():
	#	symbol=symbol.lower()
	#hybrid=str(atom.GetHybridization())
	#symbol=symbol+"_"+hybrid
	return symbol


def generate_sim_score(mol_1,mol_2,smarts):
        smiles_1=Chem.MolToSmiles(mol_1)
        smiles_2=Chem.MolToSmiles(mol_2)
        #print smiles_1,smiles_2
        best_markush=0
        if smiles_1==smiles_2:
                best_matches=mol_1.GetNumAtoms()
                best_sim_score=1.0
        elif mol_1.GetNumAtoms()==1 and smiles_1=="[H+]":
                best_matches=0
                best_sim_score=0.0
        elif mol_2.GetNumAtoms()==1 and smiles_2=="[H+]":
                best_matches=0
                best_sim_score=0.0
        elif mol_1.GetNumAtoms()==2 and smiles_1=="[HH]":
                best_matches=0
                best_sim_score=0.0
        elif mol_2.GetNumAtoms()==2 and smiles_2=="[HH]":
                best_matches=0
                best_sim_score=0.0
        else:
                if mol_1.GetNumAtoms()==1:
                        smarts=Chem.MolToSmarts(mol_1)
                elif mol_2.GetNumAtoms()==1:
                        smarts=Chem.MolToSmarts(mol_2)
                if smarts==None:
                        best_matches=0
                        best_markush=0
                        best_sim_score=0.0
                else:
                        matches_1=get_matches(mol_1,smarts)
                        matches_2=get_matches(mol_2,smarts)
                        #print "matches_1",len(matches_1)
                        #print "matches_2",len(matches_2)
                        best_sim_score=0.0
                        best_matches=0
                        best_markush=0
                        size_1=mol_1.GetNumAtoms()
                        size_2=mol_2.GetNumAtoms()

                        for match_1 in matches_1:
                                for match_2 in matches_2:
                                        #print match_1,match_2
                                        sim_score=0.0
                                        matches=0
                                        #need to count markush to gross up size_2
                                        markush=0
                                        for i in range(0,len(match_1)):
                                                atom_1=mol_1.GetAtomWithIdx(match_1[i])
                                                atom_2=mol_2.GetAtomWithIdx(match_2[i])
                                                symbol_1=get_symbol(atom_1)
                                                symbol_2=get_symbol(atom_2)
                                                if symbol_2=="*":
                                                        markush_size=get_atoms_in_markush(mol_1,match_1,atom_1)
                                                        #print "markush_size",markush_size
                                                        matches+=1
                                                        markush+=(markush_size-1)
                                                        #print "markush",markush
                                                elif symbol_1==symbol_2:
                                                        matches+=1
                                        sim_score=float(matches+markush)/float(size_1 + (size_2+markush)  - (matches+markush))
                                        #print matches,markush,sim_score
                                        if matches>best_matches:
                                                #print "*** new best"
                                                best_sim_score=sim_score
                                                best_matches=matches
                                                best_markush=markush
                                        elif matches==best_matches and sim_score>best_sim_score:
                                                #print "*** new best"
                                                best_sim_score=sim_score
                                                best_matches=matches
                                                best_markush=markush
        return (best_matches+best_markush),best_sim_score,best_markush




def get_atoms_in_markush(mol,m,atom):
        #print "markush",match
        match_list=list(m)
        match=0
        to_visit=set([atom.GetIdx()])
        visited=set()

        while len(to_visit)>0:
                next_id=list(to_visit)[0]
                match+=1
                visited.add(next_id)
                next_atom=mol.GetAtomWithIdx(next_id)
                neighbors=next_atom.GetNeighbors()
                for neigh in neighbors:
                        if neigh.GetIdx() not in match_list:
                                to_visit.add(neigh.GetIdx())
                                #print "to visit",to_visit      
                to_visit=to_visit.difference(visited)
                #print "to visit",to_visit
                #print match
        return match

def generate_sim_arr_parity(mol_1,mol_2):
	ms = [mol_1,mol_2]
	sim_arr=[None,None,None,None]
	
	#First match bond types and elements, then match any bond type and elements, then any bond type and any element to find the best score for a low similar fragment, for more similar fragments then any any will return the best match
	matches_best=None
	sim_score_best=None
	markush_best=None
	
	mcs_graph_bondtypes_elements=mcs.FindMCS(ms,bondCompare='bondtypes',atomCompare='elements',timeout=10.0,completeRingsOnly=True)
	#mcs_graph_bondtypes_elements=mcs.FindMCS(ms,bondCompare=mcs.BondCompare.CompareOrder,atomCompare=mcs.AtomCompare.CompareElements,timeout=10.0)
	matches_best,sim_score_best,markush_best=generate_sim_score(mol_1,mol_2,mcs_graph_bondtypes_elements.smarts)

	mcs_graph_any_elements=mcs.FindMCS(ms,bondCompare='any',atomCompare='elements',timeout=10.0,completeRingsOnly=True)
	matches,sim_score,markush=generate_sim_score(mol_1,mol_2,mcs_graph_any_elements.smarts)
	if sim_score>sim_score_best:
		matches_best=matches
		sim_score_best=sim_score
		markush_best=markush

	mcs_graph_any_any=mcs.FindMCS(ms,bondCompare='any',atomCompare='any',timeout=30.0,completeRingsOnly=True)
	matches,sim_score,markush=generate_sim_score(mol_1,mol_2,mcs_graph_any_any.smarts)
	if sim_score>sim_score_best:
		matches_best=matches
		sim_score_best=sim_score
		markush_best=markush

	sim_arr[0]=mol_1.GetNumAtoms()
	sim_arr[1]=mol_2.GetNumAtoms()+markush_best

	sim_arr[2]=matches_best
	sim_arr[3]=sim_score_best
	return sim_arr

def generate_sim_arr(mol_1,mol_2):
	ms = [mol_1,mol_2]
	sim_arr=[None,None,None,None]
	fps = [Chem.RDKFingerprint(x) for x in ms]
	sim = DataStructs.FingerprintSimilarity(fps[0],fps[1])
	sim_arr[0]=mol_1.GetNumAtoms()
	sim_arr[1]=mol_2.GetNumAtoms()
	sim_arr[2]=0
	sim_arr[3]=sim
	return sim_arr

def calculate_other_ratio(other):
	num_set=set()
	num_set.add(float(other[0]))
	num_set.add(float(other[1]))
	matched_other=other[2]
	
	if 0 in num_set:
		num_set.remove(0)
	if len(num_set)>0:
		denom=min(num_set)
		ratio=min(1.0,len(matched_other)/denom)
	else:
		ratio=-1
	return ratio

def get_cognate_type(pdb,ec,rxn,cog):
	return "_NA"

def print_sim_res_line(f,key,model):
	pdb=key[0]
	ec=key[1]
	ec_c=ec.split('.')[0]
	rxn=key[2]
	rxn_strip=rxn.replace('_r','').replace('_p','')
	#print(rxn,rxn_strip)
	if pdb in pdb_cluster:
		cluster_100=pdb_cluster[pdb][0]
		cluster_80=pdb_cluster[pdb][1]
	else:
		cluster_100='NA'
		cluster_80='NA'

	cpd_sim_arr=model_res[model][key][0]
	#print cpd_sim_arr
	rxn_sim=model_res[model][key][1]
	rxn_sim_bin=round(2*rxn_sim,1)/2
	is_best_pdb = model_best_pdb[model][pdb][0][0]==key
	if ec in model_best_ec[model]:
		is_best_ec = model_best_ec[model][ec][0][0]==key
	else:
		is_best_ec = False
	if rxn_strip in model_best_rxn[model]:
		is_best_rxn = model_best_rxn[model][rxn_strip][0][0]==key
	else:
		is_best_rxn = False
	if is_best_pdb or is_best_ec or is_best_rxn:
		rank=0
		prev_sim=-1
		markush=0
		for i in range(0,len(cpd_sim_arr)):
			cpd_sim=cpd_sim_arr[i]
			cognate=cpd_sim[0][1]
			if cognate.endswith('R'):
				markush=1
				break	
		for i in range(0,len(cpd_sim_arr)):
			cpd_sim=cpd_sim_arr[i]
			bound=cpd_sim[0][0]
			bound_bonds='NA'
			bound_rot='NA'
			if bound in global_bound_mol_map:
				bound_mol=global_bound_mol_map[bound]
				bound_bonds=str(bound_mol.GetNumBonds())
				bound_rot=str(Chem.rdMolDescriptors.CalcNumRotatableBonds(bound_mol))
			cognate=cpd_sim[0][1]
			cog_bonds='NA'
			cog_rot='NA'
			if cognate in global_cognate_mol_map:
				cog_mol=global_cognate_mol_map[cognate]
				cog_bonds=str(cog_mol.GetNumBonds())
				cog_rot=str(Chem.rdMolDescriptors.CalcNumRotatableBonds(cog_mol))
			cognate_type=get_cognate_type(pdb,ec,rxn,cognate)
			counts=cpd_sim[1]
			bound_num=counts[0]
			cognate_num=counts[1]
			mcs_num=counts[2]
			sim=cpd_sim[2]
			#print cpd_sim
			sim_bin=round(2*sim,1)/2
			#if sim!=prev_sim:
			rank+=1
			prev_sim=sim
			#print cognate,bound,rank
			f.write("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.3f,%.3f,%.3f,%.3f,%d,"%(pdb,ec,ec_c,rxn,bound,cognate,cognate_type,bound_num,cognate_num,mcs_num,rxn_sim,rxn_sim_bin,sim,sim_bin,rank))
			f.write("%d,%d,%d,"%(is_best_pdb,is_best_ec,is_best_rxn))
			f.write("%s,%s,%s,%s,%d\n"%(bound_rot,bound_bonds,cog_rot,cog_bonds,markush))

def update_best_maps(bestMap,model,key,res_key,res_list,score,ec_sifts):
	#print "\nupdate map",type,key,res_key,res
	if key not in bestMap[model]:
		#print "add new",res_key,mcs_res
		bestMap[model][key]=[]
	#elif score>=map[model][key][1]:
		#print "improve",mcs_res,map[type][key][1]
	insert_pos=0
	for i in range(0,len(bestMap[model][key])):
		if score <= bestMap[model][key][i][1]:
			insert_pos+=1
		else:
			break
	#print insert_pos
	bestMap[model][key].insert(insert_pos,(res_key,score,ec_sifts))
	

def generate_bound_cognate_sim(pdb,bound_mol_map,cognate_mol_map):
	bound_cognate_sim_map={}
	bound_cognate_sim_list=[]
	#print "bound",bound_mol_map
	for bound_ref in bound_mol_map:
		bound_cognate_sim_map[bound_ref]={}
		bound_mol=bound_mol_map[bound_ref]
		for cognate_ref in cognate_mol_map:
			#print "JDT",bound_ref,cognate_ref
			cognate_mol=cognate_mol_map[cognate_ref]
			key=(bound_ref,cognate_ref)

			if key in global_sim_arr_map:
				sim_arr=global_sim_arr_map[key]
			else:
				#print bound_ref,cognate_ref
				#print Chem.MolToSmiles(bound_mol)
				if useParity==1:
					sim_arr = generate_sim_arr_parity(bound_mol,cognate_mol)
				else:
					sim_arr = generate_sim_arr(bound_mol,cognate_mol)
				global_sim_arr_map[key]=sim_arr
			bound_cognate_sim_list.append((key,sim_arr[0:3],sim_arr[3]))
			bound_cognate_sim_map[bound_ref][key]=sim_arr
	#print bound_cognate_sim_list
	bound_cognate_sim_list.sort(key=lambda x: x[2],reverse=True)
	return bound_cognate_sim_list

def get_rxn_rType_map(pdb,ec):
	rxn_rType_map={}
	if ec not in ec_rxn_rType_map:
		if "-" not in ec:
			if ec not in unmatched_ec_pdb_map:
				unmatched_ec_pdb_map[ec]=set()
			unmatched_ec_pdb_map[ec].add(pdb)
	else:
		rxn_rType_map=ec_rxn_rType_map[ec]
	return rxn_rType_map

def get_cognate_map(rxn,rxn_rType_map):
	rType=rxn_rType_map[rxn]
	cognate_map={}
	cognate_map["reactant"]=set(rType[0])
	cognate_map["product"]=set(rType[1])
	return cognate_map

def generate_bound_candidates(type,cognate_map,bound_cognate_sim_map):
	bound_main=set()
	bound_other=set()
	if type=="all":
		bound_main=set(bound_cognate_sim_map.keys())
	elif type=="other_gt_main":
		for bound_ref in bound_cognate_sim_map:
			best_other_sim=0
			best_main_sim=0
			for bound_cognate in bound_cognate_sim_map[bound_ref]:
				cognate_ref=bound_cognate[2]
				sim=float(bound_cognate_sim_map[bound_ref][bound_cognate][0])
				if (cognate_ref in cognate_map['other']['r'] or cognate_ref in cognate_map['other']['p']):
					if sim >= best_other_sim:
						best_other_sim=sim
				elif (cognate_ref in cognate_map['main']['r'] or cognate_ref in cognate_map['main']['p']):
					if sim >= best_main_sim:
						best_main_sim=sim
			if best_other_sim > best_main_sim:
				bound_other.add(bound_ref)
		bound_main=set(bound_cognate_sim_map.keys()).difference(bound_other)
	else:
		sim_thresh=float(type.split('_')[1])
		for bound_ref in bound_cognate_sim_map:
			for bound_cognate in bound_cognate_sim_map[bound_ref]:
				cognate_ref=bound_cognate[2]
				sim=float(bound_cognate_sim_map[bound_ref][bound_cognate][0])
				if sim >= sim_thresh and (cognate_ref in cognate_map['other']['r'] or cognate_ref in cognate_map['other']['p']):
					bound_other.add(bound_ref)
		bound_main=set(bound_cognate_sim_map.keys()).difference(bound_other)
	return bound_main,bound_other

def get_ec_leaves(ec):
	ec_start=ec.split('-')[0]
	ec_leaves=set()
	#JDT need to include ec as some rxn reactions match to a partial ec
	ec_leaves.add(ec)
	for key in ec_rxn_rType_map:
		if key.startswith(ec_start):
			ec_leaves.add((key,ec))
	return ec_leaves

def get_cognate_mol_map(cognate_map):
	cognate_mol_map={}
	cognate_refs=set()
	for cognate_set in cognate_map.values():
		cognate_refs.update(cognate_set)
	#print(cognate_refs)
	for cognate_ref in cognate_refs:
		if cognate_ref in global_cognate_mol_map:
			cognate_mol=global_cognate_mol_map[cognate_ref]
		# R appended to cognate ref if it contains a markush, useful for excluding these later
		else:
			cognate_mol=get_cognate_mol(cognate_ref)
			#print(Chem.MolToSmiles(cognate_mol))
			if cognate_mol==None:
				continue
			contains_markush=False
			for atom in cognate_mol.GetAtoms():
				if atom.GetAtomicNum()==0:
					#print "found dummy",cognate_ref
					contains_markush=True
					cog_markush.add(cognate_ref)
					break
			global_cognate_mol_map[cognate_ref]=cognate_mol
		cognate_mol_map[cognate_ref]=cognate_mol
	return cognate_mol_map

def get_bound_mol_map(bound_name_set,pdb):
	bound_mol_map={}
	for bound_ref in bound_name_set:
		bound_mol=None
		if bound_ref in global_bound_mol_map:
			bound_mol=global_bound_mol_map[bound_ref]
		elif bound_ref in bound_smiles_map:
			bound_smiles = bound_smiles_map[bound_ref]
			bound_mol=Chem.MolFromSmiles(bound_smiles)
		if bound_mol==None:
			if bound_ref not in unmatched_bound_map:
				unmatched_bound_map[bound_ref]=set()
			unmatched_bound_map[bound_ref].add(pdb)
			continue
		bound_mol_map[bound_ref]=bound_mol
		global_bound_mol_map[bound_ref]=bound_mol
	return bound_mol_map

def filter_list_for_cpds(l,cpds):
	filter_list=[]
	matched_bound=set()
	matched_cognate=set()
	for i in range(0,len(l)):
		item=l[i]
		bound=item[0][0]
		cognate=item[0][1]
		if cognate in cpds and bound not in matched_bound and cognate not in matched_cognate:
			filter_list.append(item)
			matched_bound.add(bound)
			matched_cognate.add(cognate)
	unmatched_cognate=cpds.difference(matched_cognate)
	for cog in unmatched_cognate:
		if cog in global_cognate_mol_map:
			cog_num=global_cognate_mol_map[cog].GetNumAtoms()
		else:
			cog_num='NA'
		filter_list.append((('noMatch',cog),(0,cog_num,0),0.0))
	return filter_list

def generate_reaction_score_parity(l):
	matches=0
	atoms=0
	for i in range(0,len(l)):
		counts=l[i][1]
		if counts[1]!='NA':
			atoms+=counts[0]
			atoms+=counts[1]
			matches+=counts[2]
	rxn_score=0
	if (atoms-matches)==0:
		rxn_score=0
	else:
		rxn_score=float(matches)/float(atoms-matches)
	return rxn_score

def generate_reaction_score(l):
	cognate_atoms=0
	rxn_score=0.0
	for i in range(0,len(l)):
		counts=l[i][1]
		if counts[1]!='NA':
			cognate_atoms+=counts[1]
	for i in range(0,len(l)):
		#print(l[i])
		sim=l[i][2]
		counts=l[i][1]
		if counts[1]!='NA':
			rxn_score+=sim*counts[1]/cognate_atoms
	return rxn_score

def generate_sim_res_pdb(ec_arr,pdb,bound_name_set):
	global global_unmatched_ec
	ec_matched=0
	if pdb in pdb_cluster:
		cluster_100=pdb_cluster[pdb][0]
		cluster_80=pdb_cluster[pdb][1]
	else:
		cluster_100='NA'
		cluster_80='NA'
	#print pdb,ec_list
	ec_set=set()
	for ec_ref in ec_arr:
		ec=ec_ref.strip()
		#### Need to add downstrem EC references for partial ECs
		if '-' in ec:
			ec_leaves = get_ec_leaves(ec)
			ec_set.update(ec_leaves)
		ec_set.add((ec,ec))
		#### Need to add rxns that have partial EC refs but could match to the specific in the PDB
		#ecSplit=ec.split('.')
		#for i in range(len(ecSplit)-1,0,-1):
		#	ecSplit[i]='-'
		#	ecUpwards=".".join(ecSplit)
		#	ec_set.add((ecUpwards,ec))			
	#print "len ec_set",len(ec_set),ec_list
	#print("$$$",ec_set)
	ec_count=0
	for ec_tup in ec_set:
		ec=ec_tup[0]
		ec_sifts=ec_tup[1]
		ec_count+=1
		ec_c=ec.split('.')[0]
		rxn_rType_map=get_rxn_rType_map(pdb,ec)
		#print "JDT",ec,rxn_rType_map
		if len(rxn_rType_map)>0:
			ec_matched+=1
		#continue #JDT comment this line! ************************************************
		rxn_count=0
		for rxn in rxn_rType_map:
			#print("**",pdb,rxn,ec) 
			rxn_count+=1
			#print ec,ec_count,"out of",len(ec_set),";",rxn,rxn_count,"out of",len(rxn_rType_map)
			cognate_map=get_cognate_map(rxn,rxn_rType_map)
			#print cognate_map
			global_cognate_map[(pdb,ec,rxn)]=cognate_map
			cognate_mol_map=get_cognate_mol_map(cognate_map)
			bound_mol_map=get_bound_mol_map(bound_name_set,pdb)
			bound_cognate_sim_list=generate_bound_cognate_sim(pdb,bound_mol_map,cognate_mol_map)
			
			#print bound_cognate_sim_list
			#print cognate_map
			r_sim_list=[]
			p_sim_list=[]
			r_sim_score=None
			p_sim_score=None
			res_key=(pdb,ec,rxn)
			#print res_key
			for model in models:
				cpds,contains_markush=get_cpds_for_model(cognate_map,model)
				if len(cpds)==0:
					# found no cognate
					continue
				if contains_markush and not include_markush:
                                        continue
				sim_list=filter_list_for_cpds(bound_cognate_sim_list,cpds)
				if useParity==1:
					sim_score=generate_reaction_score_parity(sim_list)
				else:
					sim_score=generate_reaction_score(sim_list)
				#print "sim_list zzz",sim_list
				if model=='reactant':
					r_sim_list=list(sim_list)
					r_sim_score=sim_score
					rxn_adj=rxn+"_r"
				elif model=='product':
					p_sim_list=list(sim_list)
					p_sim_score=sim_score
					rxn_adj=rxn+"_p"
				res_key=(pdb,ec,rxn_adj)
					
				#print model,cpds,sim_score
				model_res[model][res_key]=(sim_list,sim_score,ec_sifts)
				#print model_res	
				update_best_maps(model_best_pdb,model,pdb,res_key,sim_list,sim_score,ec_sifts)
				#print rxn
				#if '-' not in ec: ### include partial matches
				update_best_maps(model_best_ec,model,ec,res_key,sim_list,sim_score,ec_sifts)
				update_best_maps(model_best_rxn,model,rxn,res_key,sim_list,sim_score,ec_sifts)
			#print(r_sim_score,p_sim_score)

			if r_sim_score==None and p_sim_score==None:
				continue	
			elif p_sim_score==None or (r_sim_score!=None and r_sim_score >= p_sim_score):
				best='r'
				sim_score=r_sim_score
				sim_list=r_sim_list
				#print "best r"
			else :
				best='p'
				sim_score=p_sim_score
				sim_list=p_sim_list
				#print "best p"
			rxn_adj=rxn+"_"+best
			res_key=(pdb,ec,rxn_adj)
			#print r_sim_score,p_sim_score,sim_score
			model_summary='all'
			model_res[model_summary][res_key]=(sim_list,sim_score)
			update_best_maps(model_best_pdb,model_summary,pdb,res_key,sim_list,sim_score,ec_sifts)

			update_best_maps(model_best_ec,model_summary,ec,res_key,sim_list,sim_score,ec_sifts)
			update_best_maps(model_best_rxn,model_summary,rxn,res_key,sim_list,sim_score,ec_sifts)
				
	if ec_matched==0:
		unmatched_pdb_ec_map[pdb]=ec_set

def get_cpds_for_model(cognate_map,model):
	cpds=set()
	cpds_markush=set()
	cpds_noMarkush=set()
	cpds.update(cognate_map[model])
	contains_markush=0
	for cpd in cpds:
		if cpd in cog_markush:
			cpds_markush.add(cpd)
			contains_markush=1
		else:
			cpds_noMarkush.add(cpd)
	return cpds,contains_markush

def set_to_str(s):
        str=""
        l=list(s)
        l.sort()
        for item in l:
                str=str+item+"_"
        str=str[:-1]
        return str


def tupleSet_to_str(s):
	str=""
	#print(s)
	l=list(s)
	
	for item in l:
		str=str+";".join(list(item))+"_"
	str=str[:-1]
	return str

def generate_cpd_str(cpd_sim_arr):
	cpd_str=""
	markushSet=set()
	for i in range(0,len(cpd_sim_arr[0])):
		match=cpd_sim_arr[0][i]
		bou=match[0][0]
		cog=match[0][1]
		if cog in cog_markush:
			markushSet.add(cog)
		sim=match[2]
		cpd_str=cpd_str+"%s;%s;%.3f_"%(bou,cog,sim)
	cpd_str=cpd_str[:-1]
	markush=list(markushSet)
	markush.sort()
	return cpd_str,markush
	

def print_rank(model_best,name):
	for model in model_best:
		suffix=""
		if batch!=0:
			suffix="_"+str(batch)
		model_f=open("sim/rank_stats_%s_%s%s.csv"%(name,model,suffix),"w")
		model_f.write("rank,key,pdb,ec_sifts,ec_rxnId,rxnId,r_or_p,rxn_sim,markush,cpd_str\n")
		for key in model_best[model]:
			#print(key)
			res_list=model_best[model][key]
			prev_score=None
			rank=1
			for i in range(0,len(res_list)):
				res=res_list[i]
				score=res[1]
				if score!=prev_score:
					rank=i+1
				prev_score=score	
				ec_sifts=res[2]
				cpd_sim_arr=model_res[model][res[0]]
				#print(cpd_sim_arr)
				cpd_str,markush=generate_cpd_str(cpd_sim_arr)
				pdb=res[0][0]
				ec=res[0][1]
				rxn=res[0][2].split("_")[0]
				
				r_or_p=res[0][2].split("_")[1]
				model_f.write("%d,%s,%s,%s,%s,%s,%s,%.3f,%s,%s\n"%(rank,key,pdb,ec_sifts,ec,rxn,r_or_p,score,";".join(markush),cpd_str))
		model_f.close()

def print_ranks():
	print_rank(model_best_pdb,"pdb")
	print_rank(model_best_ec,"ec")
	print_rank(model_best_rxn,"rxn")

def print_cpd_sim():
	suffix=""
	if batch!=0:
		suffix="_"+str(batch)
	f=open("sim/bound_cog_sim%s.csv"%suffix,'w')
	f.write("bound,cognate,bound_num,cognate_num,mcs_num,sim\n")
	for key in global_sim_arr_map:
		bou=key[0]
		cog=key[1]
		sim_arr=global_sim_arr_map[key]
		bou_atoms=sim_arr[0]
		cog_atoms=sim_arr[1]
		matches=sim_arr[2]
		sim=sim_arr[3]
		f.write("%s,%s,%d,%d,%d,%.3f\n"%(bou,cog,bou_atoms,cog_atoms,matches,sim))
	f.close()

def print_sim_res():
	for model in model_res:
		suffix=""
		if batch!=0:
			suffix="_"+str(batch)
		model_f=open("sim/pdb_sim_%s%s.csv"%(model,suffix),"w")
		model_f.write("pdb,ec,ec_c,rxnId,bound,cognate,cognate_type,bound_num,cognate_num,mcs_num,rxn_sim,rxn_sim_bin,cpd_sim,cpd_sim_bin,cpd_rank,")
		model_f.write("is_best_pdb,is_best_ec,is_best_rxnId,")
		model_f.write("bound_rot,bound_bonds,cog_rot,cog_bonds,markush\n")
		for key in model_res[model]:
			#print model,key
			print_sim_res_line(model_f,key,model)
		model_f.close()

def print_admin_files():
	suffix=""
	if batch!=0:
		suffix="_"+str(batch)
	error_f=open("sim/errors_pdb_sim%s.csv"%(suffix),"w")
	for pdb in error_set:
		error_f.write("%s\n"%pdb)
	error_f.close()

	unmatched_bound_f=open("sim/unmatched_bound%s.csv"%(suffix),"w")
	unmatched_bound_f.write("bound,pdb\n")
	for bound in unmatched_bound_map:
		pdb_str=set_to_str(unmatched_bound_map[bound])
		unmatched_bound_f.write("%s,%s\n"%(bound,pdb_str))
	unmatched_bound_f.close()

	unmatched_pdb_f=open("sim/unmatched_pdb%s.csv"%(suffix),"w")
	unmatched_pdb_f.write("pdb,ec\n")
	for pdb in unmatched_pdb_ec_map:
		unmatched_pdb_f.write("%s,%s\n"%(pdb,tupleSet_to_str(unmatched_pdb_ec_map[pdb])))
	unmatched_pdb_f.close()

	unmatched_ec_f=open("sim/unmatched_ec%s.csv"%(suffix),"w")
	unmatched_ec_f.write("ec,pdb\n")
	for ec in unmatched_ec_pdb_map:
		unmatched_ec_f.write("%s,%s\n"%(ec,set_to_str(unmatched_ec_pdb_map[ec])))
	unmatched_ec_f.close()

def generate_sim_res():
	# amend
	test_set=set()
	#test_set=set(['1qmu'])
	num_pdb=len(pdbEnzyme_db)
	count=0
	
	for pdb in pdbEnzyme_db:
		if len(test_set)>0 and pdb not in test_set:
			continue
		ec_arr=pdbEnzyme_db[pdb]
		
		count+=1
		if count%5==0:
			print("analysing",count,"out of",num_pdb,": num errors",len(error_set),": num missing",len(unmatched_pdb_ec_map))
		#if count==10:
		#	break

		bound_name_set=pdb_bound_db[pdb]
		#print "*",pdb,ec_arr,bound_name_set
		generate_sim_res_pdb(ec_arr,pdb,bound_name_set)
		continue	
		try:
			bound_name_set=pdb_bound_db[pdb]
			#print("*",pdb,ec_arr,bound_name_set)
			generate_sim_res_pdb(ec_arr,pdb,bound_name_set)
		except:
			print("error in",pdb)
			error_set.add(pdb)
			continue
		
	#print model_best_pdb
	print_sim_res()
	print_ranks()
	print_cpd_sim()
	print_admin_files()

def read_pdbEnzyme_toRun():
	f=open(pdbEnzyme_toRun_path,'r')
	if batch==0:
		for line in f:
			arr = line.strip().split(',')
			pdbEnzyme_db[arr[0]]=arr[1:]
	else:
		start_pos=(batch-1)*batch_size
		end_pos=start_pos+batch_size
		print("batchParams",batch,batch_size,start_pos,end_pos)
		count=0
		for line in f:
			if count>=start_pos and count<end_pos:
				arr = line.strip().split(',')
				pdbEnzyme_db[arr[0]]=arr[1:]
			if count>end_pos:
				break
			count+=1	
	f.close()
	print("num pdbEnzyme toRun",len(pdbEnzyme_db))

def generate_smiles(ref):
	mol_block_arr=get_cpd_mol_block(ref)
	mol_block,found_R=replace_R_with_H(mol_block_arr)
	mol=Chem.MolFromMolBlock(mol_block)
	smiles=Chem.MolToSmiles(mol)
	#print(smiles)

def generate_smiles_from_mol():
	mol=Chem.MolFromMolFile("../mol/CBY.mol")
	smiles=Chem.MolToSmiles(mol)
	#print(smiles)

def read_config():
	global batch_size,include_markush,db,useParity
	f=open("config.txt",'r')
	for line in f:
		arr=line.strip().split()
		if arr[0]=='batch_size':
			batch_size=int(arr[1])
		elif arr[0]=='markush':
			include_markush=int(arr[1])
		elif arr[0]=='db':
			db=arr[1]
		elif arr[0]=='parity':
			useParity=int(arr[1])
	if db!="rhea" and db!="kegg":
		print("enter db as kegg or rhea")
		exit()
	f.close()

def read_pdb_bound():
	f=open(pdb_bound_path)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(",")
		pdb_bound_db[arr[0]]=arr[1].split(";")
	f.close()

def set_up():
	global batch,eqn_path,mol_root
	batch=0
	if len(sys.argv)==2:
		batch=int(sys.argv[1])
	if batch!=0:
		read_config()
	for model in models:
		model_res[model]={}
		model_best_pdb[model]={}
		model_best_ec[model]={}
		model_best_rxn[model]={}
	for model_summary in ['all']:#['main','all']:
		model_res[model_summary]={}
		model_best_pdb[model_summary]={}
		model_best_ec[model_summary]={}
		model_best_rxn[model_summary]={}

	if db=="kegg":
		eqn_path="../data/%s/misc/equations.csv"%(db)
		mol_root="../data/%s/mols_readable/"%(db)
	elif db=="rhea":
		eqn_path="../data/%s/misc/equations.csv"%(db)
		mol_root="../data/chebi/mols_readable/"

def main():
	print("in main")
	set_up()
	read_pdbEnzyme_toRun()	
	read_pdb_bound()
	read_eqns()
	read_bound_smiles()
	print("num pdb",len(pdbEnzyme_db))
	generate_sim_res()

if __name__ == "__main__":
	main()
