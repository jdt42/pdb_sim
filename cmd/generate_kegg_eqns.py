import os
from operator import itemgetter
from rdkit import Chem
from rdkit.Chem import AllChem

### inputs
reaction_path="../data/kegg/reactions/Reaction.txt"
ko_path="../data/ko.txt"
kegg_root_path="../../keggMols/mols"
smilesDbPath="../../keggMols/out/id2smiles_db.csv"
britePath="../data/br08601.keg"
keggChebiPath="../../keggMols/out/keggChebi.csv"


### outputs
rxn_root_path="../rxns"
ec_r_path="../data/ec_keggR.csv"
equations_path="../data/Equations.txt"
eqnPath="../data/kegg/misc/equations.csv"
speciesRxn_path="../data/species_rxns_tm.csv"
G_to_C_path="../data/G_to_C.txt"
compound_to_r_map_path="../out/c_to_r_map.txt"
keggSummary_path="../out/compounds_keggSummary.txt"
ok_if_missing_path="../data/ok_if_missing.txt"
r_def_path="../out/r_def.txt"
missingCpdsPath="../data/missing_compounds.txt"
keggRheaPath="../data/keggRhea.csv"
rxnKoPathsSpeciesPath="../data/rxnKoPathsSpecies.csv"
speciesNamePath="../data/specieName.csv"
pathwaysNamePath="../data/pathwayName.csv"
orthologyNamePath="../data/orthologyName.csv"

reaction_map={}
r_def_map={}
id2smiles={}
r_ec_map={}
ec_r_map={}
keggRhea={}
keggChebiCorrections={}

pathwaysMap={}
orthologyMap={}
rxnKoPathsSpecies={}
koRxn={}
speciesKo={}
speciesRxn={}
speciesName={}
G_to_C_map={}
cToName_map={}
kegg_files=None
missing_compounds={}
ok_if_missing_set=set()

speciesToPrint=['hsa','sce','ecoli']

def reformat_array(array):
	reformat_array=[]
	for item in array:
		item=item.strip()
		if item.endswith("(n)"):
			reformat_array.append("(n) %s"%(item.replace("(n)","")))
		elif item.endswith("(m)"):
			reformat_array.append("(m) %s"%(item.replace("(m)","")))
		elif item.endswith("(n+1)"):
			reformat_array.append("(n+1) %s"%(item.replace("(n+1)","")))
		elif item.endswith("(n+m)"):
			reformat_array.append("(n+m) %s"%(item.replace("(n+m)","")))
		elif item.endswith("(n-1)"):
			reformat_array.append("(n-1) %s"%(item.replace("(n-1)","")))
		elif item.endswith("(m+n)"):
			reformat_array.append("(m+n) %s"%(item.replace("(m+n)","")))
		elif item.endswith("(n-x)"):
			reformat_array.append("(n-x) %s"%(item.replace("(n-x)","")))
		elif item.endswith("(n+2)"):
			reformat_array.append("(n+2) %s"%(item.replace("(n+2)","")))
		elif item.endswith("(x)"):
			reformat_array.append("(x) %s"%(item.replace("(x)","")))
		elif item.endswith("(m-1)"):
			reformat_array.append("(m-1) %s"%(item.replace("(m-1)","")))
		elif item.endswith("(m+1)"):
			reformat_array.append("(m+1) %s"%(item.replace("(m+1)","")))
		else:
			reformat_array.append(item)
	return reformat_array

def reformat_if_digit_prefix(array):
	return_array=array
	if len(array)==1 and array[0][0].isdigit():
		num = array[0][0]
		c_ref = array[0][1:]
		return_array=[num,c_ref]	
	return return_array

def process_equation(ref,line):
	line=line.replace("EQUATION","").replace("(in)","").replace("(out)","")
	reactants=line.split("<=>")[0]
	products=line.split("<=>")[1]
	reactants_array=reactants.split(" + ")
	products_array=products.split(" + ")
	reactants_array=reformat_array(reactants_array)
	products_array=reformat_array(products_array)
	for r in reactants_array:
		r_array=r.split()
		r_array=reformat_if_digit_prefix(r_array)
		if len(r_array)==1:
			kegg_ref=r_array[0].strip()
			if kegg_ref=="C00080":
				continue
			if kegg_ref in G_to_C_map:
				kegg_ref= G_to_C_map[kegg_ref]
			reaction_map[ref][0].append((1,kegg_ref))
		elif len(r_array)==2:
			kegg_ref=r_array[1].strip()
			if kegg_ref=="C00080":
				continue
			if kegg_ref in G_to_C_map:
				kegg_ref= G_to_C_map[kegg_ref]
			reaction_map[ref][0].append((r_array[0],kegg_ref))
	for p in products_array:
		p_array=p.split()
		p_array=reformat_if_digit_prefix(p_array)
		if len(p_array)==1:
			kegg_ref=p_array[0].strip()
			if kegg_ref=="C00080":
				continue
			if kegg_ref in G_to_C_map:
				kegg_ref= G_to_C_map[kegg_ref]
			reaction_map[ref][1].append((1,kegg_ref))
		elif len(p_array)==2:
			kegg_ref=p_array[1].strip()
			if kegg_ref=="C00080":
				continue
			if kegg_ref in G_to_C_map:
				kegg_ref= G_to_C_map[kegg_ref]
			reaction_map[ref][1].append((p_array[0],kegg_ref))
	

def update_rpair_map(ref,pair,pair_map):
	if ref not in pair_map:
		pair_map[ref]=set()
	pair_map[ref].add(pair)

def update_set_map(ref,pair,set_map):
	if ref not in set_map:
		set_map[ref]=set()
	set_map[ref].update(set(pair))

def generateSpeciesRxn():
	for species in speciesKo:
		speciesRxn[species]=set()
		kos=speciesKo[species]
		for ko in kos:
			if ko not in koRxn:
				continue
			rxns=koRxn[ko]
			speciesRxn[species].update(rxns)


def read_ko():
	f=open(ko_path)
	line=f.readline()
	count=0
	rxns=[]
	pathways=set()
	while line:
		count+=1
		if count%100000==0:
			print("reading ko.txt",count)
		if line.startswith("ENTRY"):
			ko=line.strip().split()[1]
			rxns.clear()
			pathways.clear()
			defin=""
		if line.startswith("DEFINITION"):
			defin=line.replace("DEFINITION","").replace(",",";").strip()
			orthologyMap[ko]=defin
			#print(ko,defin)
		if line.startswith("DBLINKS     RN:"):
			rxns=line.strip().split()[2:]
			#print(rxns)
			for rxn in rxns:
				if rxn not in rxnKoPathsSpecies:
					rxnKoPathsSpecies[rxn]={}
				if ko not in rxnKoPathsSpecies[rxn]:
					rxnKoPathsSpecies[rxn][ko]=[set(pathways),set()]
					#print(rxnKoPathsSpecies[rxn][ko][0])
				
		if line.startswith("PATHWAY"):
			pathway=line.strip().split()[1]
			pathwayName=line.strip().split("  ")[-1].replace(",",";")
			pathwaysMap[pathway]=pathwayName
			pathways.add(pathway)
			line=f.readline()
			while line.startswith("  "):
				pathway=line.strip().split()[0]
				pathwayName=line.strip().split("  ")[-1].replace(",",";")
				pathwaysMap[pathway]=pathwayName
				pathways.add(pathway)
				line=f.readline()
			
		if line.startswith("GENES"):
			species=line.strip().split()[1].replace(":","").lower()
			for rxn in rxns:
				rxnKoPathsSpecies[rxn][ko][1].add(species)
			if species not in speciesKo:
				speciesKo[species]=set()
			speciesKo[species].add(ko)
			line=f.readline()
			while line.startswith("  "):
				species=line.strip().split()[0].replace(":","").lower()
				for rxn in rxns:
					rxnKoPathsSpecies[rxn][ko][1].add(species)
				if species not in speciesKo:
					speciesKo[species]=set()
				speciesKo[species].add(ko)
				line=f.readline()
		line=f.readline()
	print("number speciesKo",len(speciesKo))

def read_reactionFile():
	reaction_file=open(reaction_path,"r")
	ref=""
	eq=""
	enzyme_line=False
	count=0	
	line = reaction_file.readline()
	#for line in reaction_file:
	while line:
		if line.startswith("ENTRY"):
			line_array = line.split()
			ref=line_array[1]
			reaction_map[ref]=(list(),list())
		if line.startswith("ORTHOLOGY"):
			arr=line.strip().split()
			ko=arr[1]
			if ko not in koRxn:
				koRxn[ko]=set()
			koRxn[ko].add(ref)
			line=reaction_file.readline()
			while line.startswith("  "):
				ko=line.strip().split()[0]
				if ko not in koRxn:
					koRxn[ko]=set()
				koRxn[ko].add(ref)
				line=reaction_file.readline()
		if line.startswith("EQUATION"):
			count+=1
			process_equation(ref,line)
		if line.startswith("DBLINKS"):
			rhea=line.strip().split("RHEA:")[1]
			keggRhea[ref]=rhea
		if line.startswith("DEFINITION"):
			definition=line.replace('DEFINITION','').strip()
			r_def_map[ref]=definition
		# reads subsequent enzyme lines after ENZYME label until another label is encountered
		if enzyme_line and not line.startswith(" "):
			enzyme_line=False
		if enzyme_line:
			line_array = line.split()
			for i in range(0,len(line_array)):
				ec=line_array[i].strip()
				if ec not in ec_r_map:
					ec_r_map[ec]=set()
				ec_r_map[ec].add(ref)
				r_ec_map[ref].add(ec)
		if line.startswith("ENZYME"):
			enzyme_line=True
			r_ec_map[ref]=set()
			line_array = line.split()
			for i in range(1,len(line_array)):
				ec=line_array[i].strip()
				if ec not in ec_r_map:
					ec_r_map[ec]=set()
				ec_r_map[ec].add(ref)
				r_ec_map[ref].add(ec)
		if line.startswith("///"):
			ref=""
			eq=""
			enzyme_line=False
		line=reaction_file.readline()	
	reaction_file.close()
	print("num reactions",len(reaction_map))
	print("num ko",len(koRxn))

def print_equationsTxt():
	equations_file=open(equations_path,"w")
	refs=list(reaction_map.keys())
	refs.sort()
	for i in range(0,len(refs)):
		ref=refs[i]
		reactants=reaction_map[ref][0]
		products=reaction_map[ref][1]
		equations_file.write("%s,%s,%s\n"%(ref,reactants,products)) 	
	equations_file.close()

def printEqns():
	f=open(eqnPath,'w')
	f.write("keggId,numR,numP,rs,ps,ecs\n")
	refs=list(reaction_map.keys())
	refs.sort()
	for i in range(0,len(refs)):
		ref=refs[i]
		reactants=reaction_map[ref][0]
		products=reaction_map[ref][1]
		keggRs=[x[1] for x in reactants]
		keggPs=[x[1] for x in products]
		ecs=[]
		if ref in r_ec_map:
			ecs=list(r_ec_map[ref])
			ecs.sort()
		f.write("%s,%d,%d,%s,%s,%s\n"%(ref,len(keggRs),len(keggPs),";".join(keggRs),";".join(keggPs),";".join(ecs)))
	f.close()

def count_compounds(array):
	count_found=0
	count_missing=0
	for item in array:
		ref=item[1]
		if ref in ok_if_missing_set:
			continue
		mol_ref=ref+".mol"
		if ref.startswith('G') and ref in G_to_C_map.keys():
			mol_ref=G_to_C_map[ref]+".mol"
		if mol_ref in kegg_files:
			count_found+=int(item[0])
		else:
			count_missing+=int(item[0])
	return count_found,count_missing

def read_mol_file(path):
	f=open(path,"r")
	lines=f.readlines()
	f.close()
	return lines


def isContainsMarkush(c):
	if c in id2smiles:
		return int(id2smiles[c][0])
	return 0

def print_compound(f,pair,r):
	num=int(pair[0])
	c=pair[1]
	lines=""
	if c not in keggChebiCorrections:
		mol_file=c+".mol"
		if c.startswith('G') and c in G_to_C_map.keys():
			mol_file=G_to_C_map[c]+".mol"		
		if mol_file in kegg_files:
			#print "reading kegg file"
			mol_path=kegg_root_path+"/"+mol_file
			lines=read_mol_file(mol_path)
		else:
			if c not in missing_compounds:
				missing_compounds[c]=set()
			missing_compounds[c].add(r)
		if lines!="":
			for i in range(0,num):
				f.write("$MOL\n")
				for line in lines:
					f.write('%s'%line)
					if not line.endswith("\n"):
						f.write("\n")
	else:
		mol=Chem.MolFromSmiles(keggChebiCorrections[c])
		mol.SetProp("_Name",c)
		AllChem.Compute2DCoords(mol)
		molBlock=Chem.MolToMolBlock(mol)
		f.write("$MOL\n")
		f.write(molBlock)
		print("****** correcting",r,c)
	
	containsMarkush=isContainsMarkush(c)
	return containsMarkush

def ok_to_run(eq):
	# reject equations with n,m,n+1,n_m,etc
	reactants=eq[0]
	products=eq[1]
	for reactant in reactants:
		if not str(reactant[0]).isdigit():
			return False	
	for product in products:
		if not str(product[0]).isdigit():
			return False			
	return True

def formatAcceptors(li):
	replace_map={}
	return_list=list(li)
	for item in li:
		if item[1]=="C00030":
			replace_map[item]=(item[0],"C00080")
		if item[1]=="C00028":
			replace_map[item]="delete"
	for item in replace_map:
		if replace_map[item]=="delete":
			return_list.remove(item)
		else:
			pos=return_list.index(item)
			return_list[pos]=replace_map[item]
	return return_list

def addSpaces(n,s):
	while(len(s)<n):
		s=" "+s
	return s

def print_rxn_file(r_map,path):
	global kegg_files
	kegg_files=os.listdir(kegg_root_path)
	r_list=list(reaction_map.keys())
	r_list.sort()
	#r_list=['R00623']
	count=0
	for r in r_list:
		count+=1
		#if count>20:
		#	break
		eq=r_map[r]
		#print eq
		if ok_to_run(eq):
			rxnContainsMarkush=False
			print("generating rxn file for ",r)
			reactants=eq[0]
			products=eq[1]
			
			#reactants=formatAcceptors(reactants)
			#products=formatAcceptors(products)
			
			num_r,r_missing = count_compounds(reactants)
			num_p,p_missing = count_compounds(products)
			#print "###",r_missing,p_missing
			if r_missing >0 or p_missing > 0:
				rxn_path=path+"/incomplete/"+r+".rxn"
			else:
				rxn_path=path+"/pass/"+r+".rxn"
			#print rxn_path
			rxn_file=open(rxn_path,"w")
			rxn_file.write('$RXN\n\n\n\n')
			str_r=str(num_r)
			str_p=str(num_p)
			str_r=addSpaces(3,str_r)
			str_p=addSpaces(3,str_p)
			
			rxn_file.write('%s%s\n'%(str_r,str_p))
			for reactant in reactants:
				cpdContainsMarkush=print_compound(rxn_file,reactant,r)
				if cpdContainsMarkush>0:
					rxnContainsMarkush=True
			for product in products:
				cpdContainsMarkush=print_compound(rxn_file,product,r)
				if cpdContainsMarkush>0:
					rxnContainsMarkush=True
			rxn_file.close()
			if rxnContainsMarkush:
				os.rename(rxn_path,"%s/containsMarkush/%s.rxn"%(path,r))
		else:
			rxn_path=path+"/polymer/"+r+".rxn"
			rxn_file=open(rxn_path,"w")
			rxn_file.write('$RXN\n\n\n\n')
			rxn_file.close()

def print_missing_compounds():
	f=open(missingCpdsPath,'w')
	f.write("c,ok_if_missing,numR,Rs,name\n")
	for c in missing_compounds:
		keggRs=list(missing_compounds[c])
		keggRs.sort()
		f.write('%s,%d,%d,%s\n'%(c,c in ok_if_missing_set, len(keggRs),";".join(keggRs)))
	f.close()
	

def read_G_to_C_file():
	f=open(G_to_C_path,"r")
	for line in f:
		line_array =line.split(",")
		G_to_C_map[line_array[0]]=line_array[1].strip()
	G_to_C_map

def read_ok_if_missing():
	f = open(ok_if_missing_path,'r')
	for line in f:
		ok_if_missing_set.add(line.strip())
	f.close()

def print_r_def_map():
	f=open(r_def_path,'w')
	for r in r_def_map:
		f.write('%s\t%s\n'%(r,r_def_map[r]))
	f.close()

def eliminate_type_from_eqn(typeSet,eqn):
	eqn_new=(list(),list())
	for idx in range(0,len(eqn)):
		for cpd in eqn[idx]:
			if cpd[1] not in typeSet:
				eqn_new[idx].append(cpd)	
	return eqn_new

def remove_trans_from_main_pair_set(t,main_pair_set):
	copy_set=set(main_pair_set)
	for pair in main_pair_set:
		if t in pair:
			copy_set.remove(pair)
	return copy_set

def find_real_trans_pair_in_main(t,main_pair_set):
	t_pair=tuple()
	for pair in main_pair_set:
		if t in pair:
			t_pair=pair
			break
	return t_pair

def generate_reactants(eqn):
	reactants=eqn[0]
	reactants_set=set()
	for reactant in reactants:
		reactants_set.add(reactant[1])
	return reactants_set

def generate_products(eqn):
	products=eqn[1]
	products_set=set()
	for product in products:
		products_set.add(product[1])
	return products_set

def generate_transferees_moreThanOnce(trans_pair_set,eqn):
	transferee_count_map={}
	transferee_moreThanOnce_set=set()
	for trans_pair in trans_pair_set:
		trans_pair_set=set(trans_pair)
		for c in trans_pair_set:
			if is_mol_product(c,eqn):
				if c not in transferee_count_map:
					transferee_count_map[c]=0
				transferee_count_map[c]+=1
	for c in transferee_count_map.keys():
		if transferee_count_map[c]>1:
			transferee_moreThanOnce_set.add(c)
	return transferee_moreThanOnce_set

def move_transferee_moreThanOnce_toMain(trans_pair_set,main_pair_set,transferee_moreThanOnce_set):
	trans_pair_set_copy=set(trans_pair_set)
	trans_pair_set_copy.update(main_pair_set)
	main_pair_set.clear()
	for trans_pair in trans_pair_set:
		for c in trans_pair:
			if c in transferee_moreThanOnce_set:
				trans_pair_set_copy.remove(trans_pair)
				main_pair_set.add(trans_pair)
				break
	return trans_pair_set_copy

def get_all_compounds(eqn):
	all_set=set()
	all_set.update(generate_reactants(eqn))
	all_set.update(generate_products(eqn))
	return all_set


def find_min_c(cpds):
	min_c=None
	min_f=None
	for c in cpds:
		f=get_cpd_freq(c)
		if min_c==None:
			min_c=c
			min_f=f
		elif f < min_f:
			min_c=c
			min_f=f
	return min_c


def count_reactants(eqn,c_set):
	count=0
	for c in c_set:
		if is_mol_reactant(c,eqn):
			count+=1
	return count

def count_products(eqn,c_set):
	count=0
	for c in c_set:
		if is_mol_product(c,eqn):
			count+=1
	return count

def is_mol_reactant(mol_ref,eqn):
	reactants=eqn[0]
	for reactant in reactants:
		if mol_ref == reactant[1]:
			return True
	return False

def is_mol_product(mol_ref,eqn):
	products=eqn[1]
	for product in products:
		if mol_ref == product[1]:
			return True
	return False

def update_string(string,c):
	if len(string)==0:
		return c
	else:
		return string + ";" + c

def print_keggRhea():
	f=open(keggRheaPath,'w')
	f.write("kegg,rhea\n")
	keggRs=list(keggRhea.keys())
	keggRs.sort()
	for kegg in keggRs:
		f.write("%s,%s\n"%(kegg,keggRhea[kegg]))
	f.close()

def read_smilesDb():
	f=open(smilesDbPath)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')

		id2smiles[arr[0]]=arr[1:]
	print("num id2smiles",len(id2smiles))

def print_speciesRxn():
	f=open(speciesRxn_path,'w')
	f.write("species,numRxns,Rxns\n")
	speciesRxn["ecoli"]=set()
	for species in speciesRxn:
		name="NA"
		if species in speciesName:
			name=speciesName[species][4]
		if "Escherichia coli" in name:
			speciesRxn["ecoli"].update(speciesRxn[species])
	for species in speciesRxn:
		if species in speciesToPrint:
			rxns=list(speciesRxn[species])
			rxns.sort()
			f.write("%s,%d,%s\n"%(species,len(rxns),";".join(rxns)))
	f.close()

def readSpecies():
	f=open(britePath)
	A="NA"
	B="NA"
	C="NA"
	D="NA"
	E="NA"
	for line in f:
		first=line[0]
		
		if first=="A":
			A=line.split(">")[1].split("<")[0]
		elif first=="B":
			if ">" in line:
				B=line.split(">")[1].split("<")[0]
		elif first=="C":
			C=line[1:].split("(")[0].strip()
		elif first=="D":
			D=line[1:].split("(")[0].strip()
		elif first=="E":
			arr=line.strip().split()
			species=arr[1]
			E=" ".join(arr[2:]).replace(",",";")
			#print(A,B,C,D,E)
			speciesName[species]=[A,B,C,D,E]
		elif first=="F":
			print(line)
	f.close()

def readKeggChebi():
	f=open(keggChebiPath)
	count=0
	for line in f:
		count+=1
		if count==1:
			continue
		arr=line.strip().split(',')
		kegg=arr[0]
		numReplaceSmiles=int(arr[4])
		if numReplaceSmiles==1:
			smiles=arr[5]
			keggChebiCorrections[kegg]=smiles
	f.close()
	print("num keggChebiCorrections",len(keggChebiCorrections))

def print_ec_r_map():
	f=open(ec_r_path,'w')
	f.write("ec,numKeggR,keggRs\n")
	ecs=list(ec_r_map.keys())
	ecs.sort()
	for ec in ecs:
		keggRs=list(ec_r_map[ec])
		keggRs.sort()
		f.write("%s,%s,%s\n"%(ec,len(keggRs),"_".join(keggRs)))
	f.close()

def print_rxnKoPathsSpecies():
	f=open(rxnKoPathsSpeciesPath,'w')
	f.write("rxn,ko,pathways,species\n")
	rxns=list(rxnKoPathsSpecies.keys())
	rxns.sort()
	for rxn in rxns:
		kos=list(rxnKoPathsSpecies[rxn])
		kos.sort()
		for ko in kos:
			data=rxnKoPathsSpecies[rxn][ko]
			pathways=list(data[0])
			pathways.sort()
			species=list(data[1])
			species.sort()
			f.write("%s,%s,%s,%s\n"%(rxn,ko,"_".join(pathways),"_".join(species)))
	f.close()

def print_speciesName():
	f=open(speciesNamePath,'w')
	f.write("species,A,B,C,D,E\n")
	speciesList=list(speciesName.keys())
	speciesList.sort()
	for specie in speciesList:
		f.write("%s,%s\n"%(specie,",".join(speciesName[specie])))
	f.close()

def print_pathwaysName():
	f=open(pathwaysNamePath,'w')
	f.write("pathway,name\n")
	pathwaysList=list(pathwaysMap.keys())
	pathwaysList.sort()
	for pathway in pathwaysList:
		f.write("%s,%s\n"%(pathway,pathwaysMap[pathway]))
	f.close()

def print_orthologyName():
	f=open(orthologyNamePath,'w')
	f.write("orthology,name\n")
	orthologyList=list(orthologyMap.keys())
	orthologyList.sort()
	for orthology in orthologyList:
		f.write("%s,%s\n"%(orthology,orthologyMap[orthology]))
	f.close()

def main():
	#readSpecies()
	#read_ko()
	
	#print_rxnKoPathsSpecies()
	#print_speciesName()
	#print_pathwaysName()
	#print_orthologyName()
	
	#readKeggChebi()
	#readSpecies()
	#read_smilesDb()
	#read_ok_if_missing()
	#read_G_to_C_file()
	read_reactionFile()
	#read_ko()
	#generateSpeciesRxn()
	#print_equationsTxt()
	printEqns()

	#print_rxn_file(reaction_map, rxn_root_path)
	#print_missing_compounds()

	#print_r_def_map()
	#print_keggRhea()
	#print_speciesRxn()
	#print_ec_r_map()
	
if __name__ == "__main__":
	main()
