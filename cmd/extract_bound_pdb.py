import os
import sys

from rdkit import Chem
from rdkit.Chem import AllChem
from rdkit.Chem import Descriptors
from rdkit import DataStructs
from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem import MACCSkeys
from rdkit.Chem.AtomPairs import Pairs
from rdkit.Chem.AtomPairs import Torsions
from rdkit.Chem import MCS as mcs
#from rdkit.Chem import rdFMCS as mcs

from simtk import openmm
from simtk.openmm.app import *
from simtk.openmm import *
import scipy


pdb_root=None
pdb_bound_path=None
pdb_bound_db={}

peptideBond=["C","N"]

def read_pdb_bound(pdbName):
	p='%s/%s/%s/%s.pdb'%(pdb_root,pdbName[0],pdbName[1],pdbName)
	f = open(p,'r')
	bound_resNum_set=set()
	bound_name_set=set()
	for line in f:
		if line.startswith('HETATM'):
			#print(line)
			bound_name=line[17:20].strip()
			resNum=int(line[22:26])
			chain=line[21]
			if bound_name!='HOH':
				bound_resNum_set.add((resNum,bound_name))
	f.close()
	
	try:
		pdb = PDBFile(p)
		mod=Modeller(pdb.topology, pdb.positions)
		top=pdb.topology
		#print(pdbName,bound_resNum_set)
		for res in top.residues():
			resId=int(res.id)
			k=(resId,res.name)
			if k not in bound_resNum_set:
				continue
			#print(res.name,res.index,res.chain,res.id)
			backboneBond=False
			for bond in res.external_bonds():
				atom0=bond[0]
				atom1=bond[1]
				bondList=[]
				bondList.append(atom0.name[0])
				bondList.append(atom1.name[0])
				#print(bondList)
				bondList.sort()
				if bondList==peptideBond:
					#print("***** peptideBond")
					backboneBond=True
				#print("&",pdbName,atom0.residue.name,atom0.residue.id,atom0.name,atom1.residue.name,atom1.residue.id,atom1.name)
			if not backboneBond:
				bound_name_set.add(res.name)		
			#else:
			#	print(pdbName)
			#	exit()	
		#print(pdbName,bound_name_set)
	except:
		for item in bound_resNum_set:
			bound_name_set.add(item[1]) 	
	
	return bound_name_set

def run():
	dirs1=os.listdir(pdb_root)
	#print(dirs1)
	count=0
	for d1 in dirs1:
		path1="%s/%s"%(pdb_root,d1)
		if not os.path.isdir(path1):
			continue
		dirs2=os.listdir(path1)
		#print(dirs2)
		for d2 in dirs2:
			path2="%s/%s/%s"%(pdb_root,d1,d2)
			if not os.path.isdir(path2):
				continue
			pdbs=os.listdir(path2)
			for pdb in pdbs:
				count+=1
				if count%1000==0:
					print("extracting",count)
				pdbName=pdb.replace(".pdb","")
				if pdbName in pdb_bound_db:
					continue
				bound_name_set=read_pdb_bound(pdbName)
				#print(bound_name_set)
				pdb_bound_db[pdbName]=bound_name_set
						

def read_pdb_bound_file():
	if os.path.isfile(pdb_bound_path):
		f=open(pdb_bound_path)
		count=0
		for line in f:
			count+=1
			if count==1:
				continue
			arr=line.strip().split(",")
			pdb_bound_db[arr[0]]=arr[1].split(";")
		f.close()
	print("numPdb",len(pdb_bound_db))
	

def print_pdb_bound():
	f=open(pdb_bound_path,"w")
	f.write("pdb,bound\n")
	for pdb in pdb_bound_db:
		f.write("%s,%s\n"%(pdb,";".join(pdb_bound_db[pdb])))
	f.close()
	

def set_up():
	global pdb_root, pdb_bound_path
	if len(sys.argv)!=3:
		print("enter path to pdb root and pdb_bound_path")
		exit()
	pdb_root=sys.argv[1]
	pdb_bound_path="pdb_bound.csv"
	print(pdb_root)	
	print(pdb_bound_path)

def main():
	set_up()
	read_pdb_bound_file()
	run()
	print_pdb_bound()

if __name__ == "__main__":
	main()
