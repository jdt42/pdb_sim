# python ../cmd/generate_pdb_sim_farm.py ../data/sifts/pdb_chain_enzyme_21_6_6.csv 500 0 kegg 
# python ../cmd/generate_pdb_sim_farm.py ../data/sifts/pdb_chain_enzyme_21_6_6.csv 500 0 rhea


import sys
import os
import shutil
import math

sifts_path=None
pdbEnzyme_db={}
batch_size=500
include_markush=None
db=None
useParity=0

def read_pdbEnzyme():
	f=open(sifts_path,'r')
	for line in f:
		if line.startswith("#") or line.startswith("PDB"):
			continue
		arr = line.strip().split(',')
		pdb = arr[0]
		ec = arr[3]
		if pdb not in pdbEnzyme_db:
			pdbEnzyme_db[pdb]=set()
		pdbEnzyme_db[pdb].add(ec)
	print("num of pdbs",len(pdbEnzyme_db))
	f.close()

def print_pdbEnzyme():
	f=open("pdbEnzyme_toRun.csv",'w')
	pdb_list=list(pdbEnzyme_db.keys())
	pdb_list.sort()
	for i in range(0,len(pdb_list)):
		pdb=pdb_list[i]
		f.write("%s,%s\n"%(pdb,",".join(pdbEnzyme_db[pdb])))
	f.close()

def run_farm(num_jobs):
	command='../cmd/generate_pdb_sim_farm.sh ' + str(num_jobs)
	os.system(command)

def clean():
	files = os.listdir("./")
	for f in files:
		if f=="config.txt":
			continue
		if os.path.isdir(f):
			shutil.rmtree(f)
		else:
			os.remove(f)

def read_config():
	global sifts_path,batch_size,include_markush,useParity,db
	f=open(config_path)
	for line in f:
		arr=line.strip().split()
		print(arr)
		if arr[0]=="sifts_path":
			sifts_path=arr[1]
		elif arr[0]=="batch_size":
			batch_size=int(arr[1])
		elif arr[0]=="markush":
			include_markush=int(arr[1])
		elif arr[0]=="db":
			db=arr[1]
		elif arr[0]=="parity":
			useParity=arr[1]

def set_up():
	global config_path
	if len(sys.argv)!=2:
		print("enter path to config file with sifts_path, batch size, markush 0/1 and db kegg/rhea")
		exit()
	config_path=sys.argv[1]
	read_config()
	os.mkdir("out")
	os.mkdir("err")
	os.mkdir("res")
	os.mkdir("sim")
	

def main():
	clean()
	set_up()
	read_pdbEnzyme()
	print_pdbEnzyme()
	num_jobs=math.ceil(len(pdbEnzyme_db)/batch_size)
	print("num_jobs",num_jobs)
	#num_jobs=10
	run_farm(num_jobs)

if __name__ == "__main__":
	main()
