from Bio.KEGG import REST
from Bio.KEGG import Enzyme

import os
from time import sleep

errors=set()

def download_reactions():
	reactions=[]
	reactions_txt = REST.kegg_list("reaction").read()
	reactions=reactions_txt.split("\n")
	os.system("echo "" > Reaction.txt")
	for i in range(0,len(reactions)):
		try:
			reaction=reactions[i].split()[0]
			print("downloading",i,len(reactions))
			ref=reaction.split(":")[1]
			name="%s.txt"%(ref)
			if not os.path.isfile(name):
				sleep(0.2)
				request = REST.kegg_get(reaction)
				open(name, 'w').write(request.read())
			os.system("cat %s >> Reaction.txt"%(name))		
		except:
			continue
		#if i>50:
		#	exit()		

def print_errors():
	f=open("errors.txt","w")
	for error in errors:
		f.write("%s\n"%(error))
	f.close()

def main():
	#read_species()
	#run_module("md:eci_M00446")
	download_reactions()
	#print_errors()	

if __name__=="__main__":
	main()
